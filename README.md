# Hybrid scattered-regular nodes

Conference paper for [ICCS 2023](https://www.iccs-meeting.org/iccs2023/) on numerical solution on hybrid
of regular and scattered nodes.

## Abstract

In this paper, we address a way to reduce the total computational cost of meshless approximation by reducing the required stencil size through spatial variation of computational node regularity. Rather than covering the entire domain with scattered nodes, only regions with geometric details are covered with scattered nodes, while the rest of the domain is discretised with regular nodes. Consequently, in regions covered with regular nodes the approximation using solely the monomial basis can be performed, effectively reducing the required stencil size compared to the approximation on scattered nodes where a set of polyharmonic splines is added to ensure convergent behaviour.

The performance of the proposed hybrid scattered-regular approximation approach, in terms of computational efficiency and accuracy of the numerical solution, is studied on natural convection driven fluid flow problems. We start with the solution of the de Vahl Davis benchmark case, defined on square domain, and continue with two- and three-dimensional irregularly shaped domains. We show that the spatial variation of the two approximation methods can significantly reduce the computational complexity, with only a minor impact on the solution accuracy.

## Description

[Medusa](https://e6.ijs.si/medusa/) supports different approximation engines. Commonly used are:

- the **radial basis function-generated finite differences** using the Polyharmonic splines augmented with monomials and
- the cheapest **collocation method** employed on regular nodes, but unstable on scattered nodes.

### Goal

Demonstrative [de Vahl Davis problem](https://e6.ijs.si/medusa/wiki/index.php/De_Vahl_Davis_natural_convection_test)
will be solved and computational times will be studied.

## Visuals

For progress and certain visuals please check the `/logs` and `/results` directories.

## Installation

Requirements:

- CMake
- C++ compiler
- Python 3.9 (or higher)
- Jupyter notebooks
- [Medusa project](https://gitlab.com/e62Lab/medusa)

## Usage

Create or go to `build/` directory and build using

```bash
cmake .. && make -j 12
```

The executable will be created in `bin/` directory. The executable must be run with a parameter with
all the settings, e.g.

```bash
./case ../input/devahldavis.xml 
```

This will generate a HDF file with results in the `/data` directory.

## Support

Thanks to [E62 team](https://e6.ijs.si/parlab/) for support.

## Contributing

Entire [E62 team](https://e6.ijs.si/parlab/).

## Authors and contributors

- **Mitja Jančič**  and
- **Miha Rot** and
- **Gregor Kosec**

## Acknowledgments

The authors would like to acknowledge the financial support of Slovenian Research Agency (ARRS) in the framework of the research core funding No. P2-0095 and the Young Researcher program PR-10468 and research project J2-3048.