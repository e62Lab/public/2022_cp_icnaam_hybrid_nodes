# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import scipy.spatial as spatial
import scipy.cluster.vq as cluster
import h5py
from mayavi import mlab

plt.rc('text', usetex = False)
#plt.rc('font', size = 14, family = 'serif', serif = ['Arial'])
plt.rc('font', size = 14, family = 'serif', serif = ['STIXGeneral'])
plt.rc('xtick', labelsize = 'small')
plt.rc('ytick', labelsize = 'small')
plt.rc('legend', frameon = True, fontsize = 'medium', loc = "best")
plt.rc('figure', figsize = (10, 6), facecolor = "white")
plt.rc('lines', linewidth=1.0)
plt.rc('axes', xmargin = 0, ymargin = 0, autolimit_mode = 'round_numbers')
plt.rc('mathtext', fontset = 'cm', rm = 'serif')

plt.close('all')
mlab.close(all=True)

def clover2D(phi, size=1):
    r = (1.5 - np.power(np.cos(1.5 * (phi - np.pi / 6)), 2)) / 1.5 * size
    return np.stack((r * np.cos(phi), r * np.sin(phi)))

def clover3D(phi, theta, size=1):
    r = (1.5 - np.power(np.cos(1.5 * (phi - np.pi / 6)), 2) * theta * (np.pi - theta) / 3) / 1.5 * size
    return np.stack((r * np.cos(phi) * np.sin(theta), r * np.sin(phi) * np.sin(theta), r * np.cos(theta)))

#------------------------------------------------------------------------------
dataFile = h5py.Empty(int) #to close the previous instance when working in ipython

displayIdx = -1
scatterSize = 1
with h5py.File("../data/test.h5", "r") as dataFile:
    pos = dataFile[f"domain/pos"][()]
    types = dataFile[f"domain/types"][()]
    steps = sorted(list(dataFile[f"/values"].keys()), key=lambda x: int(x.replace("step", "")))
    time = dataFile[f"/values/{steps[displayIdx]}"].attrs["time"]
    T = dataFile[f"/values/{steps[displayIdx]}/T"][()]
    v = dataFile[f"/values/{steps[displayIdx]}/v"][()]
    
    times = [dataFile[f"/values/{s}"].attrs["time"] for s in steps]
    nusselts = [np.mean(dataFile[f"/values/{s}/nusselts"][()][0]) for s in steps]
    
    fig, ax = plt.subplots()
    ax.plot(times[1:], nusselts[1:], "k.")
    
    # fig, ax = plt.subplots()
    # cold = dataFile[f"/values/{steps[displayIdx]}/nusselts"][()][0]
    # idx_cold = types == -11
    # sort_cold = np.argsort(np.arctan2(*pos[:, idx_cold]))
    # hot = dataFile[f"/values/{steps[displayIdx]}/nusselts_hot"][()][0]
    # idx_hot = types == -12
    # # ax.plot(np.arctan2(*pos[:, idx_cold][:, sort_cold]), cold)
    # ax.plot(cold)
    # ax.plot(hot)
    
    
    vAbs = np.linalg.norm(v, axis=0)
    
    if len(pos) == 3:
        mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
        qvr = mlab.points3d(*pos[:, types < 0], types[types < 0], scale_factor=0.01, scale_mode='vector')
        qvr.glyph.color_mode = 'color_by_scalar'
        mlab.outline()
        
        mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
        maxT = max(max(T), abs(min(T)))
        qvr = mlab.quiver3d(*pos, *v, mask_points=2, color=(0, 0, 0), mode="arrow",# scale_factor=0.02,
                            scalars=T, colormap="coolwarm", vmin=-maxT, vmax=maxT, opacity=1)
        qvr.glyph.color_mode = 'color_by_scalar'
        mlab.outline()
        
        # mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
        # idx = np.argsort(vAbs)[-200:]
        # # idx = np.ones_like(vAbs, dtype=bool)
        # img = mlab.points3d(*pos[:, idx], vAbs[idx], scale_factor=0.01, scale_mode='vector',
        #                     colormap="binary", opacity=0.5)
        # img.glyph.color_mode = 'color_by_scalar'
        # # img.glyph.scale_mode = 'scale_by_vector'
        # qvr = mlab.points3d(*pos[:, types < -10], types[types < -10], scale_factor=0.01, scale_mode='vector')
        # mlab.outline()
        # mlab.colorbar(img)
        # # mlab.axes()
    else:
        fig, ax = plt.subplots()
        ax.scatter(*pos, c=types, s=scatterSize)
        
        fig, ax = plt.subplots(1, 2)
        ax[0].scatter(*pos, c=np.linalg.norm(v, axis=0), s=scatterSize)
        ax[1].scatter(*pos, c=T, s=scatterSize)

#%%
fig, ax = plt.subplots(2)
# for name in ["test_fdm", "test_hybrid", "test_fdm_rbffd", "test_fdm_gauss", "test"]:
for name in ["test_hybrid", "test_gauss+0", "test_gauss+1", "test_gauss+2", "test_gauss_all+1"]:
    with h5py.File(f"../data/{name}.h5", "r") as dataFile:
        pos = dataFile[f"domain/pos"][()]
        types = dataFile[f"domain/types"][()]
        steps = sorted(list(dataFile[f"/values"].keys()), key=lambda x: int(x.replace("step", "")))
        time = dataFile[f"/values/{steps[displayIdx]}"].attrs["time"]
        
        times = [dataFile[f"/values/{s}"].attrs["time"] for s in steps]
        lastNu = dataFile[f"/values/{steps[-1]}/nusselts"][()][0]
        idxCold = dataFile["nusselt_edge_idx"]
        Nu = [np.mean(dataFile[f"/values/{s}/nusselts"][()][0]) for s in steps]
        
        ax[0].plot(times[1:], Nu[1:], ".-", label=name)
        angle = np.arctan2(*(pos[:, idxCold].T - np.average(pos[:, idxCold], axis=1)).T)
        sortIdx = np.argsort(angle)
        ax[1].plot(angle[sortIdx], lastNu[sortIdx], '.')
        ax[0].legend(ncol=2)
        
        #Could use clustering to plot angles for multiple obstructions

#%%
path = r"../bin/hybridFillTest.h5"

scatterSize = 1
bins = np.linspace(0.01, 0.015, 50)

names = ["scattered", "circle_inside", "circle_outside"]

fig, ax = plt.subplots(2, len(names), sharex="row")

with h5py.File(path, "r") as dataFile:
    col = 0
    for name in names:
        pos = dataFile[f"{name}/pos"][()]
        types = dataFile[f"{name}/types"][()]
    
        ax[0, col].scatter(*pos, s=scatterSize, c=types, cmap=plt.cm.viridis)
        
        kdtree = spatial.KDTree(pos.T)
        distances = kdtree.query(pos.T, 4)[0][:,1:]
        ax[1, col].hist(distances, bins=bins, stacked=True, label=["1st", "2nd", "3rd"])
        ax[1, col].set_yscale("log")
        ax[0, col].set_title(name)
        
        col += 1
    
    ax[0, 0].set_ylabel("$y$")
    for a in ax[0]:
        a.set_xlabel("$x$")
    ax[1, 0].set_ylabel("Count")
    for a in ax[1]:
        a.set_xlim(bins[0], bins[-1])
        a.set_xlabel("Distance to neighbour")
    
    ax[1, 0].legend()
    
# %%
path = r"../bin/hybridFillTest.h5"

scatterSize = 1
colormap = plt.cm.RdBu
colours = plt.rcParams['axes.prop_cycle'].by_key()['color']

# names = ["scattered", "circle_inside", "circle_outside"]
names = ["scattered", "clover_inside", "clover_outside"]
numNeighbour = 6
label = ["1st", "2nd", "3rd", "4th", "5th", "6th"]

with h5py.File(path, "r") as dataFile:    
    # p = dataFile[f"test/pos"][()]
    # t = dataFile[f"test/types"][()]
    # if len(p) == 3:
    #     mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
    #     qvr = mlab.points3d(*p, t, scale_factor=0.01, scale_mode='vector')
    #     qvr.glyph.color_mode = 'color_by_scalar'
    #     mlab.outline()
    # else:
    #     plt.figure()
    #     plt.scatter(*p, s=scatterSize, c=t)
    #     # for i in range(len(p[0])):
    #     #     plt.text(p[0][i], p[1][i], i)
    # raise Exception
    
    try:
        multi_pos = dataFile[f"multi_shape/pos"][()]
        multi_types = dataFile[f"multi_shape/types"][()]
    except KeyError:
        multi_pos = None
    try:
        clover_2d_pos = dataFile[f"2D/clover_surface/pos"][()]
        clover_3d_pos = dataFile[f"3D/clover_surface/pos"][()]
    except KeyError:
        clover_2d_pos = None
        clover_3d_pos = None
    
    for dim in [2, 3]:
        if dim == 2:
            fig, ax = plt.subplots(3, len(names), sharex="row", figsize=(10, 8))
            binDist = np.linspace(0.02, 0.05, 50)
            binSpac = np.linspace(0.02, 0.05, 50)
        if dim == 3:
            fig, ax = plt.subplots(2, len(names), sharex="row", figsize=(10, 5))
            binDist = np.linspace(0.02, 0.04, 50)
            binSpac = np.linspace(0.02, 0.05, 50)
            
        col = 0
        for name in names:
            row = 0
            pos = dataFile[f"{dim}D/{name}/pos"][()]
            types = dataFile[f"{dim}D/{name}/types"][()]
            kdtree = spatial.KDTree(pos.T)
            typeColour = [colours[1] if t == 2 else colours[0] for t in types]
            # scatter
            if len(pos) == 2:
                ax[row, col].scatter(*pos, s=scatterSize, c=typeColour)
                ax[row, 0].set_ylabel("$y$")
                ax[row, col].set_xlabel("$x$")
                ax[row, col].set_aspect("equal")
                row += 1
            # internodal distances
            distances = kdtree.query(pos.T, numNeighbour + 1)[0][:,1:]
            ax[row, col].hist(distances, bins=binDist, stacked=True, label=label)
            ax[row, 0].set_ylabel("Count")
            legendStep = 2
            handles, labels = ax[row, col].get_legend_handles_labels()
            ax[row, col].set_xlim(binDist[0], binDist[-1])
            ax[row, col].set_xlabel("Distance to neighbour ($d_{i, j}$)")
            ax[row, col].set_yscale("log")
            i = col * legendStep
            if i + 1 < len(handles):
                ax[row, col].legend(handles[i:i+legendStep], labels[i:i+legendStep])
            row += 1
            # empty space diameter
            voronoi = spatial.Voronoi(pos.T)
            # Find closest in tree for each -> 2*r is the radius metric
            voronoiPos = voronoi.vertices.T
            voronoiPos = voronoiPos[:, np.all(voronoiPos > 0, axis=0) * np.all(voronoiPos < 1, axis=0)]
            # ax[row, col].scatter(*voronoiPos, s=scatterSize, c="r")
            s = 2*kdtree.query(voronoiPos.T, 1)[0]
            ax[row, col].hist(s, bins=binSpac, color="k")
            ax[row, 0].set_ylabel("Count")
            ax[row, col].set_xlim(binSpac[0], binSpac[-1])
            ax[row, col].set_xlabel("Empty space diameter ($s_j$)")
            ax[row, col].set_yscale("log")
            # if dim == 3:
            #     mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
            #     qvr = mlab.points3d(*pos[:, types==1], scale_factor=0.01, scale_mode='vector')
            #     # qvr = mlab.points3d(*voronoiPos[:, s > 0.1], scale_factor=0.01, scale_mode='vector')
            #     qvr.glyph.color_mode = 'color_by_scalar'
            #     mlab.outline()
            
            ax[0, col].set_title(name.replace("_", " "))
            col += 1
    
        fig.tight_layout()
        # fig.savefig(f"../extendedPaper/images/fillStats{dim}D.pdf")

if multi_pos is not None:
    fig, ax = plt.subplots()
    ax.scatter(*multi_pos, s=scatterSize, c=[colours[1] if t == 2 else colours[0] for t in multi_types])
    ax.set_aspect("equal")
    
    
    
    edgeIdx = np.where(multi_types[1:] != multi_types[:-1])[0] + 1
    
    fig, ax = plt.subplots(1, 4, sharey=True, sharex=True, figsize=(10, 3))
    
    col = 0
    for endIdx in [edgeIdx[1] + 100, edgeIdx[2] + 150, edgeIdx[-2] + 30, -1]:
        # clover
        ax[col].plot(*(0.35 + clover2D(np.linspace(0, 2*np.pi, 1000), 0.2)), 'k-', alpha=0.5)
        # box
        ax[col].add_patch(Rectangle((0.65, 0.65 + 0.018 - np.sqrt(0.1)/2), 0.2, 0.2, 45, fill=False, color="k", linewidth=1, alpha=0.5))
        #circle
        ax[col].add_patch(Circle((0.85, 0.85), 0.1, fill=False, color="k", linewidth=1, alpha=0.5))
        ax[col].scatter(*multi_pos[:, :endIdx], s=scatterSize, c=[colours[1] if t == 2 else colours[0] for t in multi_types[:endIdx]])
        ax[col].set_aspect("equal")
        ax[col].set_xlabel("$x$")
        col += 1
    ax[0].set_ylabel("$y$")
    
    fig.tight_layout()
    # fig.savefig(f"../extendedPaper/images/fillProgression.pdf")

#TODO: - variable density on example
#      - statistics on 2 and 3 dim
#      - statistic with voronoi centres
#      - variable grid direction (the ugly way and refactor later)
#      - statistics in a table (similar to other fill papers)

# %%
if clover_3d_pos is not None:
    
    images = []
    fig = mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
    # clover_2d_pos[:, -1] = clover_2d_pos[:, 0]
    # mlab.plot3d(clover_2d_pos[0], clover_2d_pos[1], np.zeros_like(clover_2d_pos[0]), tube_radius=None)
    clover = clover2D(np.linspace(0, 2*np.pi, 1000))
    mlab.plot3d(clover[0], clover[1], np.zeros_like(clover[0]), tube_radius=None, line_width=3)
    mlab.view(0, 0, "auto", "auto")
    images.append(mlab.screenshot(fig, mode="rgba", antialiased=True))
    
    fig = mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
    # mesh = spatial.Delaunay(clover_3d_pos.T)
    # triangles = []
    # for s in mesh.simplices:
    #     reject = False
    #     for i in range(len(s)):
    #         for j in range(len(s)):
    #             if i == j: continue
    #             d = np.linalg.norm(clover_3d_pos[:, s[i]] - clover_3d_pos[:, s[j]])
    #             if d > 0.05:
    #                 reject = True
    #                 break
    #         if reject:
    #             break
    #     else:
    #         for j in range(len(s)):
    #             triangles.append([s[i] for i in range(len(s)) if i != j])
    # mlab.triangular_mesh(*clover_3d_pos, triangles)
    
    # pts = mlab.pipeline.scalar_scatter(*clover_3d_pos)
    # vol = mlab.pipeline.user_defined(pts, filter='SurfaceReconstructionFilter')
    # mlab.pipeline.iso_surface(vol, contours=[0], colormap="Accent")
    
    phi, theta = np.mgrid[0:2*np.pi:100j, 0:np.pi:100j]
    cloverPos = clover3D(phi, theta)
    mlab.mesh(*cloverPos, colormap="coolwarm")
    
    mlab.view(40, 30, "auto", "auto")
    images.append(mlab.screenshot(fig, mode="rgb"))
    
    
    fig1, ax1 = plt.subplots(1, len(images), figsize=(10, 4))
    limx = [[200, 800], [700, 200]]
    limy = [[200, 800], [700, 200]]
    
    for i in range(len(images)):
        fig, ax = plt.subplots()
        ax.imshow(images[i])
        ax.set_xlim(*limx[i])
        ax.set_ylim(*limx[i])
        ax.set_axis_off()
        # fig.subplots_adjust(wspace=0, hspace=0)
        # if i == 0:
        #     ax1[i].plot(*clover2D(np.linspace(0, 2*np.pi, 1000)), c="k")
        ax1[i].imshow(images[i])
        ax1[i].set_xlim(*limx[i])
        ax1[i].set_ylim(*limx[i])
        ax1[i].set_axis_off()
    # fig1.subplots_adjust(wspace=0, hspace=0)
    fig1.tight_layout()
    
    # fig1.savefig(f"../extendedPaper/images/obstructionShape.pdf")
    
    
    # vtk_source =  mlab.pipeline.scalar_scatter(*clover_3d_pos, figure=False)
    # delaunay =  mlab.pipeline.delaunay2d(vtk_source)
    # edges = mlab.pipeline.extract_edges(delaunay)
    # mlab.pipeline.surface(edges, opacity=0.3, line_width=3)
    # mlab.outline()

# %%
path = "../data/test_3d_timed_hyb.h5"
displayIdx = -1
nCluster = 4
obstrType = -10
with h5py.File(path, "r") as dataFile:    
    p = dataFile[f"domain/pos"][()]
    t = dataFile[f"domain/types"][()]
    steps = sorted(list(dataFile[f"/values"].keys()), key=lambda x: int(x.replace("step", "")))
    v = dataFile[f"/values/{steps[displayIdx]}/v"][()]
    T = dataFile[f"/values/{steps[displayIdx]}/T"][()]

obstructions = p[:, t < obstrType]
c, labels = cluster.kmeans2(obstructions.T, nCluster)

# mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
# for i in range(nCluster):
#     pts = mlab.pipeline.scalar_scatter(*obstructions[:, labels == i])
#     vol = mlab.pipeline.user_defined(pts, filter='SurfaceReconstructionFilter')
#     mlab.pipeline.iso_surface(vol, contours=[0], color=(0.86, 0.86, 0.86))
    
# qvr = mlab.quiver3d(*p, *v, mask_points=2, color=(0, 0, 0), mode="arrow",# scale_factor=0.02,
#                     scalars=T, colormap="coolwarm", opacity=1)
# qvr.glyph.color_mode = 'color_by_scalar'
# mlab.outline()

from scipy.spatial.transform import Rotation as R
phi, theta = np.mgrid[0:2*np.pi:100j, 0:np.pi:100j]

mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
for i in range(nCluster):
    # pts = mlab.pipeline.scalar_scatter(*obstructions[:, labels == i])
    # vol = mlab.pipeline.user_defined(pts, filter='SurfaceReconstructionFilter')
    # mlab.pipeline.iso_surface(vol, contours=[0], color=(0.86, 0.86, 0.86), opacity=0.5)
    
    maxDistIdx = np.argmax(np.linalg.norm((obstructions[:, labels == i].T - c[i]), axis=1))
    minDistIdx = np.argmin(np.linalg.norm((obstructions[:, labels == i].T - c[i]), axis=1))
    vMinData = obstructions[:, labels == i][:, minDistIdx].T - c[i]
    mlab.points3d(*obstructions[:, labels == i], color=(0.8, 0.8, 0.8), opacity=0.5, scale_factor=0.01)
    # mlab.points3d(*obstructions[:, labels == i][:, [maxDistIdx, minDistIdx]], scale_factor=0.01)
    maxd = 0
    for p in obstructions[:, labels == i].T:
        diff = p - obstructions[:, labels == i].T
        normDiff = np.linalg.norm(diff, axis=1)
        im = np.argmax(normDiff)
        if normDiff[im] > maxd:
            maxd = normDiff[im]
            polar = diff[im]
    
    cloverPos = clover3D(phi, theta, np.linalg.norm(obstructions[:, labels == i][:, [maxDistIdx]].T - c[i]))
    flat = cloverPos.T.reshape(-1, 3)
    minDistRepr = np.argmin(np.linalg.norm(flat, axis=1))
    vMinRepr = flat[minDistRepr, :]
    
    r = R.align_vectors([vMinData, polar], [vMinRepr, [0, 0, 1]])[0]
    mlab.mesh(*(r.apply(flat).reshape((100, 100, 3)) + c[i]).T, opacity=0.5)
    # mlab.points3d(*(polar/2 + c[i]), scale_factor=0.01, color=(0, 0, 1))
    # mlab.points3d(*c[i], scale_factor=0.02)

# r = spatial.transform.Rotation([1, 0, 0, 0])

# # mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))

# # mlab.mesh(*(cloverPos.T + c[-1]).T, opacity=0.5, color=(0.8, 0.8, 0.8))

# v1 = obstructions[:, labels == i][:, [maxDistIdx, minDistIdx]].T - c[i]
# flat = cloverPos.T.reshape(-1, 3)
# maxDistRepr = np.argmax(np.linalg.norm(flat, axis=1))
# minDistRepr = np.argmin(np.linalg.norm(flat, axis=1))
# mlab.points3d(*(c[i] + flat[[maxDistRepr, minDistRepr], :]).T, scale_factor=0.01, color=(1, 0, 0))
# v2 = flat[[maxDistRepr, minDistRepr], :]

# phi, theta = np.mgrid[0:2*np.pi:100j, 0:np.pi:100j]

# r = R.align_vectors([v1[0], polar/2], [v2[0], [0, 0, 1]])[0]
# # r.apply(flat).reshape((100, 100, 3))
# mlab.mesh(*(r.apply(flat).reshape((100, 100, 3)) + c[-1]).T, opacity=0.5)
    
    
    