# -*- coding: utf-8 -*-
import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
from mayavi import mlab
import scipy.spatial as spatial
import scipy.cluster.vq as cluster
from scipy.spatial.transform import Rotation as R

# Import data.
with h5.File('../data/test_3d_timed_hyb.h5') as data:
    attrs = dict(data.attrs)
    positions = data['domain/pos'][:]
    types = data['domain/types'][:]
    groups = list(data['values'].keys())
    group_ids = [int(g.split('step')[-1]) for g in groups]
    groups = ['step{}'.format(i) for i in sorted(group_ids)]
    temperature_fields = [data['values/{}/T'.format(group)][:] for group in groups]
    velocity_fields = [data['values/{}/v'.format(group)][:] for group in groups]
    times = [data['values/{}'.format(group)].attrs['time']for group in groups]
    rbffd_nodes = data['rbffd_nodes'][:]
    collocation_nodes = data['collocation_nodes'][:]
    super_collocation_nodes = data['collocation_nodes_all'][:]

marker_size=1

last_idx = times.index(max(times))
v = velocity_fields[last_idx]
T = temperature_fields[last_idx]
h = attrs["num.h"]

nCluster = 4
obstrType = -10

# %% porous3D - 3d visualisation of a vertical convection with obstructions
obstructions = positions[:, types < obstrType]
c, labels = cluster.kmeans2(obstructions.T, nCluster)

mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1), size=(1000, 1000))
for i in range(nCluster):
    pts = mlab.pipeline.scalar_scatter(*obstructions[:, labels == i])
    # vol = mlab.pipeline.user_defined(pts, filter='SurfaceReconstructionFilter')
    # mlab.pipeline.iso_surface(vol, contours=[0], color=(0.86, 0.86, 0.86))
    mesh = mlab.pipeline.delaunay3d(pts)
    surf = mlab.pipeline.surface(mesh, color=(0.86, 0.86, 0.86))
qvr = mlab.quiver3d(*positions, *v, mask_points=1, color=(0, 0, 0), mode="arrow",# scale_factor=0.02,
                    scalars=T, colormap="seismic", opacity=1)
qvr.glyph.color_mode = 'color_by_scalar'
mlab.outline()

raise Exception
print(mlab.view())
mlab.screenshot()
base_angle = -90
offset = 60
angles = [base_angle - offset, base_angle, base_angle + offset]
zooms = [2.63, 2.37, 2.63]
images = []
for i in range(len(angles)):
    mlab.view(angles[i], 90, zooms[i], "auto")
    images.append(mlab.screenshot(mode="rgb"))

fig, ax = plt.subplots(1, len(images), figsize=(10, 3.5))
for i in range(len(images)):
    ax[i].imshow(images[i])
    ax[i].set_axis_off()
fig.tight_layout()

# fig.savefig('../extendedPaper/images/irregular_3d_solution_mlab_all.png', dpi=300)

# %%
import numpy as np
from mayavi import mlab
import h5py as h5
from mlabtex import mlabtex
import scipy.cluster.vq as cluster
from scipy.spatial.transform import Rotation as R

filenames = {"scattered" : '../data/test_3d_timed_scat.h5',
             "hybrid" : '../data/test_3d_timed_hyb.h5'}

nCluster = 4
obstrType = -10
last_idx = -1
plotOffset = 1.1
textOrient = [90, 105, 0]
origin = np.array([-1.5, 0, 0])
first = True
mlab.figure(fgcolor=(0, 0, 0), bgcolor=(1, 1, 1), size=(2000, 1000))
for name, filename in filenames.items():
    with h5.File(filename) as data:
        attrs = dict(data.attrs)
        pos = data['domain/pos'][:]
        types = data['domain/types'][:]
        groups = list(data['values'].keys())
        group_ids = [int(g.split('step')[-1]) for g in groups]
        groups = ['step{}'.format(i) for i in sorted(group_ids)]
        temperature_fields = [data['values/{}/T'.format(group)][:] for group in groups]
        velocity_fields = [data['values/{}/v'.format(group)][:] for group in groups]
        v = velocity_fields[last_idx]
        T = temperature_fields[last_idx]    

    obstructions = pos[:, types < obstrType]
    c, labels = cluster.kmeans2(obstructions.T, nCluster)
    for i in range(nCluster):
        colour = [0.8, 0.8, 0.8]
        if types[types < obstrType][np.where(labels == i)[0][0]] == -12:
            colour[0] = 1
        else:
            colour[2] = 1
        # p = mlab.points3d(*(origin + obstructions[:, labels == i].T).T, color=tuple(colour))
        maxDistIdx = np.argmax(np.linalg.norm((obstructions[:, labels == i].T - c[i]), axis=1))
        minDistIdx = np.argmin(np.linalg.norm((obstructions[:, labels == i].T - c[i]), axis=1))
        vMinData = obstructions[:, labels == i][:, minDistIdx].T - c[i]
        maxd = 0
        for p in obstructions[:, labels == i].T:
            diff = p - obstructions[:, labels == i].T
            normDiff = np.linalg.norm(diff, axis=1)
            im = np.argmax(normDiff)
            if normDiff[im] > maxd:
                maxd = normDiff[im]
                polar = diff[im]
        
        cloverPos = clover3D(phi, theta, np.linalg.norm(obstructions[:, labels == i][:, [maxDistIdx]].T - c[i]))
        flat = cloverPos.T.reshape(-1, 3)
        minDistRepr = np.argmin(np.linalg.norm(flat, axis=1))
        vMinRepr = flat[minDistRepr, :]
        
        r = R.align_vectors([vMinData, polar], [vMinRepr, [0, 0, 1]])[0]
        p = mlab.mesh(*(origin + r.apply(flat).reshape((100, 100, 3)) + c[i]).T, color=tuple(colour))
        p.actor.actor.rotate_z(45)
    qvr = mlab.quiver3d(*(pos.T + origin).T, *v, mask_points=1, color=(0, 0, 0), mode="arrow",# scale_factor=0.02,
                        scalars=T, colormap="seismic", opacity=1)
    qvr.glyph.color_mode = 'color_by_scalar'
    o = mlab.outline(qvr)
    qvr.actor.actor.rotate_z(45)
    o.actor.actor.rotate_z(45)
    bndX, bndY, bndZ = qvr.actor.actor.x_range, qvr.actor.actor.y_range, qvr.actor.actor.z_range
    center = np.array(qvr.actor.actor.center)
    baseCenter = center
    baseCenter[-1] = 0
    # for x in bndX:
    #     for y in bndY:
    #         for z in bndZ:
    #             t = mlab.text3d(x, y, z, f"({x:.2f}, {y:.2f}, {z:.2f})", scale = 0.01)
    
    if first:
        tex = mlabtex(*((baseCenter + [bndX[1], bndY[0], bndZ[0]])/2 + [0, -0.075, -0.075]), "$x$", scale=0.075, orientation=textOrient)
        tex = mlabtex(*((baseCenter + [bndX[1], bndY[1], bndZ[0]])/2 + [0, 0, -0.075]), "$y$", scale=0.07, orientation=textOrient)
        tex = mlabtex(*((np.array([bndX[0], bndY[0], bndZ[0]]) + [bndX[1], bndY[0], bndZ[1]])/2  + [0, -0.075, -0.075]), "$z$", scale=0.075, orientation=textOrient)
        first = False
    
    tex = mlabtex(*((np.array([bndX[0], bndY[0], bndZ[0]]) + [bndX[1], bndY[0], bndZ[0]])/2  + [0, -0.05, -0.05]), "$0$", scale=0.05, orientation=textOrient)
    tex = mlabtex(*((np.array([bndX[1], bndY[0], bndZ[0]]) + [bndX[1], bndY[1], bndZ[0]])/2  + [0, -0.05, -0.05]), "$1$", scale=0.05, orientation=textOrient)
    tex = mlabtex(*((np.array([bndX[0], bndY[1], bndZ[0]]) + [bndX[1], bndY[1], bndZ[0]])/2  + [0, 0, -0.05]), "$1$", scale=0.05, orientation=textOrient)
    tex = mlabtex(*((np.array([bndX[0], bndY[0], bndZ[1]]) + [bndX[1], bndY[0], bndZ[1]])/2  + [0, -0.05, 0]), "$1$", scale=0.05, orientation=textOrient)
    origin[0] += plotOffset
    origin[1] += plotOffset
    # a = mlab.axes(qvr, xlabel='x',ylabel='y',zlabel='z', ranges=[0, 1, 0, 1, 0, 1], opacity=0)
cb = mlab.colorbar(qvr, nb_labels=2)
tex = mlabtex(*[-0.75, 0.65, 1.15], "$T$", scale=0.055, orientation=textOrient)
cb.scalar_bar.unconstrained_font_size = True
cb.label_text_property.font_family = 'times'
cb.label_text_property.bold = 0
cb.label_text_property.font_size=36
cb.scalar_bar_representation.position = [0.3, 0.9]
cb.scalar_bar_representation.position2 = [0.4, 0.05]

mlab.view(azimuth=15, elevation=90, distance=3.2)
mlab.move(right=0.2, up=-0.09)

# mlab.savefig('../extendedPaper/images/3D_irregular_solution_combined.png')