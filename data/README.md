# Raw data

Raw results from simulations.

Raw data can also be downloaded from [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7694759.svg)](https://doi.org/10.5281/zenodo.7694759).