#include <iostream>
#include "medusa/Medusa.hpp"

#include "helpers/DeVahlDavis.h"
#include "helpers/IrregularObstacle.h"

using namespace mm;
using namespace std;

int main(int argc, char* argv[]) {
    // Check for settings hdf.
    assert_msg(argc >= 2, "Second argument should be the XML parameter hdf.");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Set threads number.
#if defined(_OPENMP)
    omp_set_num_threads(conf.get<int>("sys.num_threads"));
#endif

    cout << "----------" << endl;
    cout << "Computing, please wait ..." << endl;

    try {
        Timer timer;
        timer.addCheckPoint("start");

        const int dim = conf.get<int>("domain.dim");
        const string problem_id = conf.get<string>("problem.id");
        if (problem_id == "dvd") {
            // De Vahl Davis.
            switch (dim) {
                case 2: {
                    // Two-dimensional setup.
                    Timer t;
                    DVD<2> solution(conf, timer, false);
                    solution.run();
                    break;
                }
                case 3: {
                    // three-dimensional setup.
                    Timer t;
                    DVD<3> solution(conf, timer, false);
                    solution.run();
                    break;
                }
                default:
                    std::cout << dim << "-dimensional DVD case has not been implemented (yet)."
                              << std::endl;
            }
        } else if (problem_id == "irregular") {
            // Irregular
            switch (dim) {
                case 2: {
                    // Two-dimensional setup.
                    Timer t;
                    IrregularObstacle<mm::Vec2d> solution(conf, timer, false);
                    solution.run();
                    break;
                }
                case 3: {
                    // Two-dimensional setup.
                    Timer t;
                    IrregularObstacle<mm::Vec3d> solution(conf, timer, false);
                    solution.run();
                    break;
                }
                default:
                    std::cout << dim << "-dimensional DVD case has not been implemented(yet)."
                              << std::endl;
            }
        } else {
            assert_msg(false, "Problem id %s not recognized. Try 'dvd' or 'sphere'.", problem_id);
        }

        // Timer stop.
        timer.addCheckPoint("end");
        prn(timer.duration("start", "end"));
    } catch (...) {
        cout << "Failed to finish with configuration: " << endl;
        prn(conf);
    };

    return 0;
}
