#include <iostream>
#include "medusa/Medusa.hpp"
#include "helpers/HybridFill.hpp"

int main(int argc, char* argv[]) {
    typedef mm::Vec2d vec_t;

    double h = 0.01;

    mm::BoxShape<vec_t> shape(vec_t::Constant(0), vec_t::Constant(1));
    mm::DomainDiscretization<vec_t> domain = shape.discretizeBoundaryWithStep(h);

    mm::HDF hdf_out;
    hdf_out.open("hybridFillTest.h5", "/", mm::HDF::DESTROY);
    mm::HybridFill<vec_t> hybrid_fill;
    mm::GeneralFill<vec_t> fill;

    auto circle_outside = [=](vec_t& p) { return (p - vec_t::Constant(0.5)).norm() > 0.4; };

    hybrid_fill(domain, h, circle_outside, 1, 2);
    hdf_out.writeDomain("circle_outside", domain);

    auto circle_inside = [=](vec_t& p) { return (p - vec_t::Constant(0.5)).norm() < 0.4; };

    domain.removeNodes(domain.interior());
    hybrid_fill(domain, h, circle_inside, 1, 2);
    hdf_out.writeDomain("circle_inside", domain);

    domain.removeNodes(domain.interior());
    fill(domain, h);
    hdf_out.writeDomain("scattered", domain);

    hdf_out.close();

    return 0;
}
