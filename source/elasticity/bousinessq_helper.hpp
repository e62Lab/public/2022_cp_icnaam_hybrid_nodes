#ifndef ICCS_HYBRID_NODES_BOUSINESSQ_HELPER_HPP
#define ICCS_HYBRID_NODES_BOUSINESSQ_HELPER_HPP

#include "medusa/Medusa.hpp"
#include "Eigen/IterativeLinearSolvers"
#include "math_helper.hpp"
#include "HybridFill.hpp"
#include "DiscretizationHelper.hpp"

using namespace mm;

// Protects against write collisions when executing parallel runs with same target file.

class Boussinesq {
  protected:
    typedef double scal_t;
    typedef Vec<scal_t, 3> vec_t;
    Timer& tim_;         // Timer.
    const XML& xml_in_;  // Configuration file.
    HDF hdf_out_;        // Output file.

    int verbosity_;  // Output verbosity.
    int mon_order_, phs_order_;
    int ss_rbffd_, ss_collocation_, ss_super_collocation_;  // Engine support sizes.
    double sigma_;
    scal_t h_large_, h_min_;
    scal_t E_, P_, nu_, lam_, mu_, a_, eps_, R_;
    string discretization_type_;
    int scattered_type_, collocation_type_;

    DomainDiscretization<vec_t> domain_;
    std::tuple<mm::Lap<mm::Vec3d::dim>, mm::Der1s<mm::Vec3d::dim>, mm::Der2s<mm::Vec3d::dim>>
        operators_;
    mm::RaggedShapeStorage<vec_t, decltype(operators_)> storage_;
    Eigen::SparseMatrix<double, Eigen::RowMajor> M_;
    Eigen::VectorXd rhs_;
    //    ExplicitOperators<decltype(storage_)> op_scalar_;
    ImplicitVectorOperators<decltype(storage_), decltype(M_), decltype(rhs_)> op_vector_;

    VectorField<scal_t, vec_t::dim> u_;
    VectorField<double, 6> stress_;
    Range<int> interior_;
    Range<Range<int>> engine_indices_;
    enum Engine : int { COLLOCATION, SUPERCOL, RBFFD, engine_count };

  public:
    explicit Boussinesq(const XML& xml_params, Timer& timer)
        : xml_in_{xml_params}, tim_{timer}, domain_(UnknownShape<vec_t>()) {
        HDF::Mode mode = xml_in_.get<bool>("output.clear_hdf5") ? HDF::DESTROY : HDF::APPEND;
        hdf_out_.open(xml_in_.get<std::string>("output.hdf5_name"), "/", mode);

        // Load variables.
        loadVariables();
    }

    ~Boussinesq() { hdf_out_.close(); }

  private:
    void loadVariables() {
        cout << "Loading params..." << endl;

        tim_.addCheckPoint("bou_start");
        // Internodal distance.
        h_large_ = xml_in_.get<scal_t>("num.h_large");
        h_min_ = xml_in_.get<scal_t>("num.h_min");

        // Output verbosity.
        verbosity_ = xml_in_.get<int>("output.verbosity");

        // COLLOCATION.
        sigma_ = xml_in_.get<double>("approx_collocation.sigma");
        auto n_collocation = xml_in_.get<int>("approx_collocation.support_size");
        if (n_collocation == -1) {
            n_collocation = 2 * vec_t::dim + 1;
        }
        ss_collocation_ = n_collocation;
        // RBF_FD
        mon_order_ = xml_in_.get<int>("approx_rbffd.mon_order");
        phs_order_ = xml_in_.get<int>("approx_rbffd.phs_order");
        int n_rbffd = xml_in_.get<int>("approx_rbffd.support_size");
        if (n_rbffd == -1) {
            n_rbffd = 2 * binomialCoeff(mon_order_ + vec_t::dim, vec_t::dim) + 1;
        }
        ss_rbffd_ = n_rbffd;
        // SUPERCOLLOCATION
        ss_super_collocation_ = ss_collocation_;

        prn(ss_rbffd_);
        prn(ss_collocation_);
        prn(ss_super_collocation_);

        // Physical parameters
        E_ = xml_in_.get<scal_t>("phy.E");
        nu_ = xml_in_.get<scal_t>("phy.nu");
        P_ = -xml_in_.get<scal_t>("phy.P");

        // Derived parameters
        lam_ = E_ * nu_ / (1 - 2 * nu_) / (1 + nu_);
        mu_ = E_ / 2 / (1 + nu_);

        // Domain.
        discretization_type_ = xml_in_.get<string>("domain.type");
        a_ = xml_in_.get<scal_t>("domain.a");
        eps_ = xml_in_.get<scal_t>("domain.eps");
        R_ = xml_in_.get<scal_t>("domain.corner_radius");
        scattered_type_ = xml_in_.get<int>("domain.scattered_type");
        collocation_type_ = xml_in_.get<int>("domain.collocation_type");

        prn(lam_);
        prn(mu_)
    }

    void constructDomain() {
        if (verbosity_ > 0) {
            cout << "Constructing domain..." << endl;
        }
        // Fill domain with nodes.
        HybridFill<vec_t> hybrid_fill;
        hybrid_fill.seed(xml_in_.template get<int>("domain.seed"));

        auto grid_f = [&](vec_t& p) {
            if (discretization_type_ == "scattered") {
                return false;
            } else if (discretization_type_ == "hybrid") {
                return ((vec_t::Constant(-eps_) - p).cross(vec_t{0, 0, 1})).norm() > R_;
            }
            return true;
        };
        // Nodal density.
        auto var_h = [&](vec_t p) {
            double dist = ((vec_t::Constant(-eps_) - p).cross(vec_t{0, 0, 1})).norm();
            if (discretization_type_ == "fdm") {
                return h_large_;
            } else {
                return std::min(h_large_, h_min_ + (h_large_ - h_min_) * dist / R_);
            };
        };
        domain_ = variableBox<vec_t>(-a_, -eps_, var_h, grid_f, -scattered_type_,
                                     -collocation_type_, xml_in_.template get<int>("domain.seed"));
        hybrid_fill(domain_, var_h, grid_f, scattered_type_, collocation_type_);

        if (xml_in_.get<bool>("domain.no_corner")) {
            removeCornerNodes();
        }

        auto borders = domain_.shape().bbox();
        scal_t d = h_min_ / 2;
        Range<int> closeToBorder;
        for (int i : domain_.interior()) {
            vec_t p = domain_.pos(i);
            bool close_to_box =
                (((p - borders.first) < d).size() + ((borders.second - p) < d).size()) > 0;
            if (close_to_box) closeToBorder.push_back(i);
        }
        domain_.removeNodes(closeToBorder);
        prn(closeToBorder);

        if (verbosity_ > 0) {
            cout << "N = " << domain_.size() << endl;
        }
    }

    void removeCornerNodes() {
        auto borders = this->domain_.shape().bbox();
        Range<int> corner = this->domain_.positions().filter([&](const vec_t& p) {
            // remove nodes that are EPS close to more than 1 border
            return (((p - borders.first).cwiseAbs() < 1e-10).size() +
                    ((borders.second - p).cwiseAbs() < 1e-10).size()) > 1;
        });
        this->domain_.removeNodes(corner);
    }

    void constructApproximation() {
        if (verbosity_ > 0) {
            cout << "Building approximation..." << endl;
        }
        tim_.addCheckPoint("approximation");

        //        Monomials<vec_t> alt_mon(1), alt_mon_all(2);
        //        alt_mon = Monomials<vec_t>(
        //            {{0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {2, 0, 0}, {0, 2, 0}, {0, 0,
        //            2}});
        //        WLS<Monomials<vec_t>, NoWeight<vec_t>, ScaleToClosest>
        //        engine_collocation(alt_mon); WLS<Monomials<vec_t>, NoWeight<vec_t>,
        //        ScaleToClosest> engine_collocation_all(alt_mon_all);
        WLS<Gaussians<vec_t>, NoWeight<vec_t>, ScaleToClosest> engine_collocation(
            {ss_collocation_, sigma_});
        WLS<Gaussians<vec_t>, NoWeight<vec_t>, ScaleToClosest> engine_collocation_all(
            {ss_super_collocation_, sigma_});

        // *************
        // RBF-FD engine.
        // *************
        mm::RBFFD<Polyharmonic<scal_t>, vec_t, ScaleToClosest> engine_rbffd(phs_order_, mon_order_);

        prn(ss_collocation_);
        prn(ss_rbffd_);
        prn(ss_super_collocation_);

        // *********
        // Supports.
        // *********
        if (verbosity_ > 0) {
            std::cout << "Finding supports..." << std::endl;
            std::cout << "-------" << std::endl;
        }

        for (int i = 0; i != engine_count; i++) {
            Engine engine = static_cast<Engine>(i);
            int n = 0;
            if (engine == COLLOCATION) {
                n = ss_collocation_;
            } else if (engine == SUPERCOL) {
                n = ss_super_collocation_;
            } else if (engine == RBFFD) {
                n = ss_rbffd_;
            }

            if (!engine_indices_[engine].empty()) {
                indexes_t engine_boundary, engine_interior;
                engine_boundary.empty();
                engine_interior.empty();
                for (int j : engine_indices_[engine]) {
                    if (domain_.type(j) < 0)
                        engine_boundary.push_back(j);
                    else
                        engine_interior.push_back(j);
                }

                if (!engine_interior.empty()) {
                    domain_.findSupport(FindClosest(n).forNodes(engine_interior));
                }
                if (!engine_boundary.empty()) {
                    domain_.findSupport(FindClosest(n)
                                            .forNodes(engine_boundary)
                                            .searchAmong(domain_.interior())
                                            .forceSelf());
                }
            }
        }

        if (verbosity_ > 0) {
            std::cout << "Computing operators" << std::endl;
        }

        // **********
        // Operators.
        // **********
        storage_.resize(domain_.supportSizes());
        // TODO: not good for multiple engines.
        if (!engine_indices_[COLLOCATION].empty()) {
            computeShapes(domain_, engine_collocation, engine_indices_[COLLOCATION], operators_,
                          &storage_);
        }
        if (!engine_indices_[SUPERCOL].empty()) {
            computeShapes(domain_, engine_collocation_all, engine_indices_[SUPERCOL], operators_,
                          &storage_);
        }
        if (!engine_indices_[RBFFD].empty()) {
            computeShapes(domain_, engine_rbffd, engine_indices_[RBFFD], operators_, &storage_);
        }

        int N = domain_.size();
        M_.resize(3 * N, 3 * N);
        rhs_.resize(3 * N);
        rhs_.setZero();
        M_.reserve(storage_.supportSizesVec());

        // Operators.
        op_vector_ = storage_.implicitVectorOperators(M_, rhs_);
    }

    void setProblem() {
        if (verbosity_ > 0) {
            cout << "Setting interior..." << endl;
        }
        // Interior.
        for (int i : domain_.interior()) {
            (lam_ + mu_) * op_vector_.graddiv(i) + mu_* op_vector_.lap(i) = 0.0;
        }

        if (verbosity_ > 0) {
            cout << "Setting boundary conditions..." << endl;
        }

        // BC.
        for (int i : domain_.boundary()) {
            op_vector_.value(i) = analytical(domain_.pos(i), P_, E_, nu_);
        }
    }

    void solveWithBicgstab() {
        if (verbosity_ > 0) {
            cout << "Solving with BiCGSTAB..." << endl;
        }
        Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>, Eigen::IncompleteLUT<double>>
            solver;
        solver.setMaxIterations(xml_in_.get<int>("solver.max_iter"));
        solver.setTolerance(xml_in_.get<double>("solver.global_tol"));
        if (xml_in_.get<bool>("solver.preconditioner.use")) {
            solver.preconditioner().setFillfactor(xml_in_.get<int>("solver.preconditioner.fill"));
            solver.preconditioner().setDroptol(
                xml_in_.get<double>("solver.preconditioner.drop_tol"));
        }
        solver.compute(M_);
        Eigen::VectorXd sol = solver.solve(rhs_);
        u_ = VectorField<scal_t, vec_t::dim>::fromLinear(sol);

        if (verbosity_ > 0) {
            prn(solver.iterations());
            prn(solver.error());
        }
    }

    void postprocess() {
        if (verbosity_ > 0) {
            cout << "Postprocessing..." << endl;
        }
        auto eop = storage_.explicitVectorOperators();
        stress_.resize(domain_.size(), 6);
        for (int i = 0; i < domain_.size(); ++i) {
            Eigen::Matrix3d grad = eop.grad(u_, i);
            Eigen::Matrix3d eps = 0.5 * (grad + grad.transpose());
            Eigen::Matrix3d s =
                lam_ * eps.trace() * Eigen::Matrix3d::Identity(3, 3) + 2 * mu_ * eps;
            stress_[i][0] = s(0, 0);
            stress_[i][1] = s(1, 1);
            stress_[i][2] = s(2, 2);
            stress_[i][3] = s(0, 1);
            stress_[i][4] = s(0, 2);
            stress_[i][5] = s(1, 2);
        }
    }

    void storeInitial() {
        if (verbosity_ > 0) {
            cout << "Storing intitial..." << endl;
        }
        hdf_out_.reopen();
        hdf_out_.openGroup("/");
        hdf_out_.writeDomain("domain", domain_);
        hdf_out_.writeXML("", xml_in_, true);
        Range<Range<int>> supports = domain_.supports();
        int max_support_size = max(ss_collocation_, ss_rbffd_);
        Range<Range<int>> equal_sized_supports;
        for (int i = 0; i < supports.size(); ++i) {
            Range<int> temp_support(max_support_size, -1);
            for (int j = 0; j < supports[i].size(); ++j) {
                temp_support[j] = supports[i][j];
            }

            equal_sized_supports.push_back(temp_support);
        }
        hdf_out_.template writeInt2DArray("supports", equal_sized_supports);
        hdf_out_.template writeIntArray("rbffd_nodes", engine_indices_[RBFFD]);
        hdf_out_.template writeIntArray("collocation_nodes", engine_indices_[COLLOCATION]);
        hdf_out_.template writeIntArray("collocation_nodes_all", engine_indices_[SUPERCOL]);
        hdf_out_.close();
    }

    void storeFinal() {
        if (verbosity_ > 0) {
            cout << "Storing final..." << endl;
        }
        tim_.addCheckPoint("bou_end");
        hdf_out_.reopen();
        hdf_out_.openGroup("/");
        hdf_out_.writeEigen("disp", u_);
        hdf_out_.writeEigen("stress", stress_);
        hdf_out_.writeTimer("timer", tim_);
        hdf_out_.close();
    }
    void assignApproximationEngine() {
        engine_indices_.resize(engine_count);
        if (discretization_type_ == "scattered") {
            this->engine_indices_[this->RBFFD] = domain_.all();
        } else if (discretization_type_ == "fdm") {
            // FDM-like.
            for (int j = 0; j < domain_.size(); ++j) {
                if (domain_.type(j) < 0) {
                    this->engine_indices_[this->SUPERCOL].push_back(j);
                } else {
                    this->engine_indices_[this->COLLOCATION].push_back(j);
                }
            }
        } else {
            domain_.findSupport(FindClosest(this->ss_collocation_).forNodes(domain_.interior()));
            domain_.findSupport(
                FindClosest(this->ss_super_collocation_).forNodes(domain_.boundary()));

            for (int j = 0; j < domain_.size(); ++j) {
                // Check if any scattered type in support.
                auto support = domain_.support(j);
                auto support_idxs = (domain_.types()[support]).asRange().filter([&](int i) {
                    return i == scattered_type_ || i == -scattered_type_;
                });
                bool scattered = !support_idxs.empty();
                domain_.support(j).clear();

                // Hybrid.
                if (scattered) {
                    this->engine_indices_[this->RBFFD].push_back(j);
                } else if (domain_.type(j) < 0) {
                    this->engine_indices_[this->SUPERCOL].push_back(j);
                } else {
                    this->engine_indices_[this->COLLOCATION].push_back(j);
                }
            }
        }
    }

  public:
    vec_t analytical(const vec_t& p, double P, double E, double nu) {
        double x = p[0], y = p[1], z = p[2];
        double r = std::sqrt(x * x + y * y);
        double c = x / r, s = y / r;
        double R = p.norm();
        double mu = E / 2 / (1 + nu);
        double u = P * r / 4 / mm::PI / mu * (z / R / R / R - (1 - 2 * nu) / (R * (R + z)));
        double w = P / 4 / mm::PI / mu * (z * z / R / R / R + 2 * (1 - nu) / R);

        return vec_t{u * c, u * s, w};
    };

    void solveProblem() {
        constructDomain();
        assignApproximationEngine();
        constructApproximation();
        storeInitial();
        setProblem();
        solveWithBicgstab();
        postprocess();
        storeFinal();
    }
};

#endif  // ICCS_HYBRID_NODES_BOUSINESSQ_HELPER_HPP
