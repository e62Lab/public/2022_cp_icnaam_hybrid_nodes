#include <medusa/Medusa.hpp>
#include "elasticity/bousinessq_helper.hpp"

using namespace mm;
using namespace std;
using namespace Eigen;

int main(int argc, char* argv[]) {
    Timer timer;
    timer.addCheckPoint("start");
    // Check for settings hdf.
    assert_msg(argc >= 2, "Second argument should be the XML parameter hdf.");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Set num threads.
#if defined(_OPENMP)
    omp_set_num_threads(conf.get<int>("sys.threads"));
#endif

    cout << "----------" << endl;
    cout << "Computing, please wait ..." << endl;
    // Run hp-adaptive.
    Boussinesq problem(conf, timer);
    problem.solveProblem();

    timer.addCheckPoint("end");
    prn(timer.duration("start", "end"));

    return 0;
}
