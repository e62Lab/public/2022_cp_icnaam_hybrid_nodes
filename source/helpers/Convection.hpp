#ifndef HYBRID_SCATTERED_UNIFORM_MIXEDCONVECTION_HPP
#define HYBRID_SCATTERED_UNIFORM_MIXEDCONVECTION_HPP

#include "medusa/Medusa.hpp"
#include <iostream>
#include "Eigen/SparseCore"
#include "Eigen/SparseLU"
#include "medusa/IO.hpp"
#include <mutex>
#include "CalculationEnvironmentBase.hpp"
#include "math_helper.hpp"

using namespace mm;

#define EPS 1e-10  // small number

// Protects against write collisions when executing parallel runs with same target file.
std::mutex global_mutex;  // Global mutex ain't pretty but solves templating issues

template <int dim>
class Convection : public CalculationEnvironmentBase {
  protected:
    typedef double scal_t;
    typedef Vec<scal_t, dim> vec_t;
    typedef std::tuple<Der1s<dim>, Lap<dim>> operator_t;
    typedef RaggedShapeStorage<vec_t, operator_t> storage_t;

    Timer& tim_;         // Timer.
    const XML& xml_in_;  // Configuration file.
    HDF hdf_out_;        // Output file.

    int verbosity_;  // Output verbosity.
    int support_size_rbffd_, support_size_collocation_,
        support_size_supercol_;  // Engine support sizes.
    double sigma_;
    int mon_order_, phs_order_;
    scal_t h_, dt_, Ra_, Pr_, Re_;
    scal_t max_norm = 1;
    int div_iter_;

    Vec<scal_t, dim> g_;

    DomainDiscretization<vec_t> domain_;
    storage_t storage_;
    ExplicitOperators<storage_t> op_scalar_;
    ExplicitVectorOperators<storage_t> op_vector_;

    ScalarFieldd T_1_, T_2_;
    VectorField<scal_t, dim> u_1_, u_2_;
    Range<Eigen::Matrix<scal_t, dim, dim>> vel_grad_;
    Range<int> interior_;
    Range<Range<int>> edge_indices_;
    enum Edge : int { HOT, COLD, NEUTRAL, type_count };
    Range<Range<int>> engine_indices_;
    enum Engine : int { COLLOCATION, SUPERCOL, RBFFD, engine_count };
    Range<int> ghost_mapping_;
    Eigen::VectorXd condition_numbers_;

    Range<scal_t> times_;

    Eigen::VectorXd rhs_;
    Eigen::SparseLU<Eigen::SparseMatrix<scal_t>> solver_;

  public:
    explicit Convection(const XML& xml_params, Timer& timer, bool mixed_convection)
        : xml_in_{xml_params}, tim_{timer}, domain_(UnknownShape<vec_t>()) {
        HDF::Mode mode = xml_in_.get<bool>("output.clear_hdf5") ? HDF::DESTROY : HDF::APPEND;
        hdf_out_.open(xml_in_.get<std::string>("output.hdf5_name"), "/", mode);
    }

    ~Convection() { hdf_out_.close(); }

    void initialize() override {
        tim_.addCheckPoint("init");

        cout << "Loading params." << endl;
        loadVariables();

        if (this->verbosity_ > 0) {
            cout << "Time stepping initialization." << endl;
        }
        initializeTimeStepping(dt_, xml_in_.get<scal_t>("case.end_time"));
        if (this->verbosity_ > 0) {
            cout << "Storage initialization." << endl;
        }
        initializeStorage(xml_in_.get<int>("output.count"));
        if (this->verbosity_ > 0) {
            cout << "Printout initialization." << endl;
        }
        initializePrintout(xml_in_.get<int>("output.printout_interval"));

        if (this->verbosity_ > 0) {
            cout << "Constructing domain." << endl;
        }
        constructDomain(domain_);
        if (this->verbosity_ > 1) {
            prn(domain_.size());
        }

        if (this->verbosity_ > 0) {
            cout << "Constructing approximation." << endl;
        }
        constructApproximation();

        if (this->verbosity_ > 0) {
            cout << "Setting initial values." << endl;
        }
        setInitialValues();

        std::cout << "h=" << h_ << ", dt=" << dt_ << ", N=" << domain_.size() << std::endl;

        tim_.addCheckPoint("start iteration");
    }

    void loadVariables() {
        // Rayleigh and Prandtl.
        Ra_ = xml_in_.get<scal_t>("case.Ra");
        Pr_ = xml_in_.get<scal_t>("case.Pr");
        Re_ = std::sqrt(Ra_ / Pr_);
        // Internodal distance.
        h_ = xml_in_.get<scal_t>("num.h");

        // Time step.
        dt_ = xml_in_.get<scal_t>("num.timestep_factor") * 0.5
                * (xml_in_.exists("num.sphere_h") ? xml_in_.get<double>("num.sphere_h") : h_);

        // Gravity.
        g_.setZero();
        g_[dim - 1] = -1;

        // Pressure correction iterations.
        div_iter_ = xml_in_.template get<int>("num.iter");

        // Output verbosity.
        verbosity_ = xml_in_.get<int>("output.verbosity");

        // COLLOCATION
        auto n_collocation = xml_in_.get<int>("approx_collocation.support_size");
        if (n_collocation == -1) {
            n_collocation = 2 * vec_t::dim + 1;
        }
        support_size_collocation_ = n_collocation;
        sigma_ = xml_in_.get<double>("approx_collocation.sigma");
        // SUPERCOLLOCATION
        support_size_supercol_ = support_size_collocation_;
        // RBF-FD
        mon_order_ = xml_in_.get<int>("approx_rbffd.mon_order");
        phs_order_ = xml_in_.get<int>("approx_rbffd.phs_order");
        int n_rbffd = xml_in_.get<int>("approx_rbffd.support_size");
        if (n_rbffd == -1) {
            n_rbffd = 2 * binomialCoeff(mon_order_ + vec_t::dim, vec_t::dim) + 1;
        }
        support_size_rbffd_ = n_rbffd;
    }

    virtual void constructDomain(DomainDiscretization<vec_t>& domain) {}

    virtual void constructApproximation() {
        tim_.addCheckPoint("approximation");

        // *******************
        // Collocation engine.
        // *******************
        auto aug = xml_in_.get<int>("approx_rbffd.mon_order");

        //        Monomials<vec_t> alt_mon(1), alt_mon_all(2);
        //        if constexpr (vec_t::dim == 2) {
        //            alt_mon = Monomials<vec_t>({{0, 0}, {1, 0}, {0, 1}, {2, 0}, {0, 2}});
        //        } else if (vec_t::dim == 3) {
        //            alt_mon = Monomials<vec_t>(
        //                {{0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {2, 0, 0}, {0, 2, 0}, {0, 0,
        //                2}});
        //        }
        //        WLS<Monomials<vec_t>, NoWeight<vec_t>, ScaleToClosest>
        //        engine_collocation(alt_mon); WLS<Monomials<vec_t>, NoWeight<vec_t>,
        //        ScaleToClosest> engine_collocation_all(alt_mon_all);
        WLS<Gaussians<vec_t>, NoWeight<vec_t>, ScaleToClosest> engine_collocation(
            {support_size_collocation_, sigma_});
        WLS<Gaussians<vec_t>, NoWeight<vec_t>, ScaleToClosest> engine_collocation_all(
            {support_size_supercol_, sigma_});

        // *************
        // RBF-FD engine.
        // *************
        mm::RBFFD<Polyharmonic<scal_t>, vec_t, ScaleToClosest> engine_rbffd(phs_order_, mon_order_);

        prn(support_size_rbffd_);
        prn(support_size_collocation_);
        prn(support_size_supercol_);

        // *********
        // Supports.
        // *********
        if (verbosity_ > 0) {
            std::cout << "Finding supports..." << std::endl;
            std::cout << "-------" << std::endl;
        }

        for (int i = 0; i != engine_count; i++) {
            Engine engine = static_cast<Engine>(i);
            int n = 0;
            if (engine == COLLOCATION) {
                n = support_size_collocation_;
            } else if (engine == SUPERCOL) {
                n = support_size_supercol_;
            } else if (engine == RBFFD) {
                n = support_size_rbffd_;
            }

            if (!engine_indices_[engine].empty()) {
                indexes_t engine_boundary, engine_interior;
                engine_boundary.empty();
                engine_interior.empty();
                for (int j : this->engine_indices_[engine]) {
                    if (domain_.type(j) < 0)
                        engine_boundary.push_back(j);
                    else
                        engine_interior.push_back(j);
                }

                if (!engine_interior.empty()) {
                    domain_.findSupport(FindClosest(n).forNodes(engine_interior));
                }
                if (!engine_boundary.empty()) {
                    domain_.findSupport(FindClosest(n)
                                            .forNodes(engine_boundary)
                                            .searchAmong(domain_.interior())
                                            .forceSelf());
                }
            }
        }

        if (verbosity_ > 0) {
            std::cout << "Computing operators" << std::endl;
        }

        // **********
        // Operators.
        // **********
        storage_.resize(domain_.supportSizes());
        operator_t operators;
        // TODO: not good for multiple engines.
        if (!engine_indices_[COLLOCATION].empty()) {
            computeShapes(domain_, engine_collocation, engine_indices_[COLLOCATION], operators,
                          &storage_);
        }
        if (!engine_indices_[SUPERCOL].empty()) {
            computeShapes(domain_, engine_collocation_all, engine_indices_[SUPERCOL], operators,
                          &storage_);
        }
        if (!engine_indices_[RBFFD].empty()) {
            computeShapes(domain_, engine_rbffd, engine_indices_[RBFFD], operators, &storage_);
        }
        op_scalar_ = storage_.explicitOperators();
        op_vector_ = storage_.explicitVectorOperators();

        // ******************************
        // INITIALIZE pressure correction
        // ******************************
        if (verbosity_ > 0) {
            std::cout << "Initialize pressure correction." << std::endl;
        }

        int N = domain_.size();
        int N_matrix = N + 1;  // +1 due to normalization.
        Eigen::SparseMatrix<scal_t, Eigen::RowMajor> M(N_matrix, N_matrix);
        rhs_.resize(N_matrix);
        rhs_.setZero();

        // Operators.
        auto op_implicit = storage_.implicitOperators(M, rhs_);

        // Set for interior.
        for (int i : domain_.interior()) {
            op_implicit.lap(i).eval(1);
        }

        // Set for boundary.
        for (int i = 0; i != type_count; i++) {
            Edge edge = static_cast<Edge>(i);

            for (int j : edge_indices_[edge]) {
                op_implicit.neumann(j, domain_.normal(j)) = 0.0;
            }
        }

        // Regularization - set the last row and column of the matrix.
        for (int i = 0; i < N; ++i) {
            M.coeffRef(N, i) = 1;
            M.coeffRef(i, N) = 1;
        }
        // Set the sum of all values.
        rhs_[N] = 0.0;

        // Compute matrix.
        M.makeCompressed();
        solver_.compute(M);
        if (solver_.info() != Eigen::Success) {
            std::cout << "LU factorization failed with error:" << solver_.lastErrorMessage()
                      << std::endl;
        }

        // **********************
        // Get condition numbers.
        // **********************
        if (xml_in_.get<bool>("output.condition_number")) {
            getConditionalNumbers(engine_rbffd, engine_collocation, engine_collocation_all);
        }
    }

    template <typename approx_rbffd, typename approx_wls, typename approx_wls_all>
    void getConditionalNumbers(approx_rbffd engine_rbffd, approx_wls engine_wls,
                               approx_wls_all engine_wls_all) {
        if (verbosity_ > 0) {
            std::cout << "Computing condition numbers." << std::endl;
        }

        condition_numbers_.resize(domain_.size());
        condition_numbers_.setZero();

        if (!engine_indices_[RBFFD].empty()) {
            for (int j : engine_indices_[RBFFD]) {
                auto point = domain_.pos(j);
                auto support = domain_.supportNodes(j);
                engine_rbffd.compute(point, support);
                engine_rbffd.getShape(Lap<vec_t::dim>{});

                double inv_norm =
                    (engine_rbffd.getMatrix().inverse()).template lpNorm<Eigen::Infinity>();
                double norm = engine_rbffd.getMatrix().template lpNorm<Eigen::Infinity>();

                condition_numbers_[j] = inv_norm * norm;
            }
        }

        if (!engine_indices_[COLLOCATION].empty()) {
            for (int j : engine_indices_[COLLOCATION]) {
                auto point = domain_.pos(j);
                auto support = domain_.supportNodes(j);
                engine_wls.compute(point, support);
                engine_wls.getShape(Lap<vec_t::dim>{});

                double inv_norm = (engine_wls.getMatrix().inverse()).template lpNorm<2>();
                double norm = engine_wls.getMatrix().template lpNorm<2>();

                condition_numbers_[j] = inv_norm * norm;
            }
        }
        if (!engine_indices_[SUPERCOL].empty()) {
            for (int j : engine_indices_[SUPERCOL]) {
                auto point = domain_.pos(j);
                auto support = domain_.supportNodes(j);
                engine_wls_all.compute(point, support);
                engine_wls_all.getShape(Lap<vec_t::dim>{});

                double inv_norm = (engine_wls_all.getMatrix().inverse()).template lpNorm<2>();
                double norm = engine_wls_all.getMatrix().template lpNorm<2>();
                condition_numbers_[j] = inv_norm * norm;
            }
        }
        if (verbosity_ > 0) {
            std::cout << "condition numbers computed." << std::endl;
        }
    }

    virtual void setInitialValues() {
        // Resize.
        T_1_.resize(domain_.size());
        u_1_.resize(domain_.size(), dim);

        // Set values.
        u_1_.setZero();
        T_1_.setZero();
        if (!edge_indices_[COLD].empty()) {
            T_1_[edge_indices_[COLD]] = xml_in_.get<double>("case.T_cold");
        }
        if (!edge_indices_[HOT].empty()) {
            T_1_[edge_indices_[HOT]] = xml_in_.get<double>("case.T_hot");
        }

        T_2_ = T_1_;
        u_2_ = u_1_;

        vel_grad_.resize(domain_.size());
    }

    void timeStep() override {
        // *************
        // Navier-Stokes.
        // *************
        // TODO: Nath added a factor 2 for mixed? Others not. WTF.
#pragma omp parallel for default(none) schedule(static)
        for (int i = 0; i < domain_.size(); ++i) {
            int c = i;
            u_2_[c] =
                u_1_[c] + dt_ * (op_vector_.lap(u_1_, c) / Re_ -
                                 op_vector_.grad(u_1_, c) * u_1_[c] - g_ * T_1_[c]);
        }

        // ********************
        // Pressure correction.
        // ********************
        ScalarFieldd p;
        max_norm = 0;
        for (int iter = 0; iter < div_iter_; ++iter) {
#pragma omp parallel for default(none) schedule(static)
            for (int i = 0; i < interior_.size(); ++i) {
                int c = interior_[i];
                rhs_(c) = op_vector_.div(u_2_, c) / dt_;
            }
            // Set for boundary.
            for (int i = 0; i != type_count; i++) {
                Edge edge = static_cast<Edge>(i);

                for (int j : edge_indices_[edge]) {
                    rhs_[j] = u_2_[j].dot(domain_.normal(j)) / dt_;
                    //                    if (ghost_mapping_[j] > 0) {
                    //                        rhs_[ghost_mapping_[j]] = 0;
                    //                    }
                }
            }

            p = solver_.solve(rhs_).head(domain_.size());

#pragma omp parallel for default(none) shared(p) schedule(static) reduction(max : max_norm)
            for (int i = 0; i < interior_.size(); ++i) {
                int c = interior_[i];
                u_2_[c] -= dt_ * op_scalar_.grad(p, c);
                max_norm = std::max(u_2_[c].norm(), max_norm);
            }

            if (iter == 0) {
                for (int i = 0; i < edge_indices_.size(); ++i) {
                    for (int j : edge_indices_[i]) {
                        u_2_[j].setZero();
                    }
                }
            }
        }

        // ***********************
        // Explicit heat transfer.
        // ***********************
#pragma omp parallel for default(none) schedule(static)
        for (int i = 0; i < interior_.size(); ++i) {
            int c = interior_[i];
            T_2_[c] = T_1_[c] + dt_ * (op_scalar_.lap(T_1_, c) / Re_ / Pr_ -
                                       u_2_[c].transpose() * op_scalar_.grad(T_1_, c));
        }

        // ********************
        // Boundary conditions.
        // ********************
        // Temperature derivative.
        if (!edge_indices_[NEUTRAL].empty()) {
#pragma omp parallel for default(none) schedule(static)
            for (int i = 0; i < edge_indices_[NEUTRAL].size(); ++i) {
                int c = edge_indices_[NEUTRAL][i];
                T_2_[c] = op_scalar_.neumann(T_2_, c, domain_.normal(c), 0.0);
            }
        }

        T_1_.swap(T_2_);
        u_1_.swap(u_2_);
    }

    bool terminateAtStoreIntermediate() override {
        for (int i = 0; i < u_2_.rows(); ++i) {
            for (int j = 0; j < dim; ++j) {
                if (std::isnan(u_2_.coeff(i, j))) {
                    std::cout << "nan node " << i << std::endl;
                    return true;
                }
            }
        }
        return false;
    };

    std::string printout() override {
        std::stringstream ss;
        ss << std::setprecision(5) << "T: min=" << T_2_.minCoeff() << " max=" << T_2_.maxCoeff()
           << " avg=" << T_2_.mean() << ", mean u:" << u_2_.mean();
        return ss.str();
    }

    Eigen::VectorXd calculateNusselt(Edge edge) {
        int N = edge_indices_[edge].size();
        Eigen::VectorXd nusselts(N);

        for (int i = 0; i < N; i++) {
            auto c = edge_indices_[edge][i];
            nusselts[i] = std::abs(op_scalar_.grad(T_2_, c).dot(domain_.normal(c)));
        }
        return nusselts;
    }

    void storeStep(int step) {
        this->hdf_out_.reopen();
        this->hdf_out_.openGroup("/values/step" + std::to_string(step));
        this->hdf_out_.writeDoubleAttribute("time", this->time_);
        this->hdf_out_.writeDoubleArray("T", this->T_2_);
        this->hdf_out_.writeEigen("v", this->u_2_);
        if (edge_indices_[COLD].size() != 0) {
            Eigen::VectorXd nusselts = calculateNusselt(COLD);
            this->hdf_out_.writeDoubleAttribute("nusselt", nusselts.mean());
            this->hdf_out_.template writeEigen("nusselts", nusselts);
        }
        if (edge_indices_[HOT].size() != 0) {
            Eigen::VectorXd nusselts = calculateNusselt(HOT);
            this->hdf_out_.template writeEigen("nusselts_hot", nusselts);
        }
        // Store gradients.
        VectorField<scal_t, dim> gradients(domain_.size());

        for (int i : domain_.all()) {
            gradients[i] = op_scalar_.grad(T_2_, i);
        }
        this->hdf_out_.writeEigen("gradients", gradients);

        this->hdf_out_.close();
    }

    void storeInitial() override {
        std::lock_guard<std::mutex> lock(global_mutex);
        this->times_.push_back(this->time_);

        this->hdf_out_.reopen();
        this->hdf_out_.openGroup("/");
        this->hdf_out_.writeDomain("domain", domain_);
        this->hdf_out_.writeXML("", this->xml_in_, true);
        //        this->hdf_out_.template writeInt2DArray("supports", domain_.supports());
        Range<Range<int>> supports = domain_.supports();
        int max_support_size = max(support_size_collocation_, support_size_rbffd_);
        Range<Range<int>> equal_sized_supports;
        for (int i = 0; i < supports.size(); ++i) {
            Range<int> temp_support(max_support_size, -1);
            for (int j = 0; j < supports[i].size(); ++j) {
                temp_support[j] = supports[i][j];
            }

            equal_sized_supports.push_back(temp_support);
        }
        this->hdf_out_.template writeInt2DArray("supports", equal_sized_supports);
        this->hdf_out_.template writeIntArray("rbffd_nodes", this->engine_indices_[this->RBFFD]);
        this->hdf_out_.template writeIntArray("collocation_nodes",
                                              this->engine_indices_[this->COLLOCATION]);
        this->hdf_out_.template writeIntArray("collocation_nodes_all",
                                              this->engine_indices_[this->SUPERCOL]);
        this->hdf_out_.template writeIntArray("nusselt_edge_idx", edge_indices_[COLD]);
        this->hdf_out_.template writeIntArray("nusselt_hot_edge_idx", edge_indices_[HOT]);
        if (xml_in_.get<bool>("output.condition_number")) {
            this->hdf_out_.template writeEigen("condition_number_lap", condition_numbers_);
        }

        this->hdf_out_.template writeIntArray("bnd_hot", edge_indices_[HOT]);
        this->hdf_out_.template writeIntArray("bnd_cold", edge_indices_[COLD]);
        this->hdf_out_.template writeIntArray("bnd_neutral", edge_indices_[NEUTRAL]);
        this->hdf_out_.close();

        storeStep(0);
    }

    void storeIntermediate(int step) override {
        std::lock_guard<std::mutex> lock(global_mutex);
        this->times_.push_back(this->time_);
        storeStep(step);
    }

    void storeFinal(int step) override {
        std::lock_guard<std::mutex> lock(global_mutex);
        this->times_.push_back(this->time_);

        this->tim_.addCheckPoint("end iteration");
        storeStep(step);
        this->hdf_out_.reopen();
        this->hdf_out_.openGroup("/");
        this->hdf_out_.writeDoubleArray("times", this->times_);
        this->hdf_out_.writeEigen("pressure", solver_.solve(rhs_).head(domain_.size()));
        this->hdf_out_.writeDoubleAttribute("run_time",
                                            this->tim_.duration("init", "end iteration"));
        this->hdf_out_.close();
    }
};

#endif  // HYBRID_SCATTERED_UNIFORM_CONVECTION_HPP
