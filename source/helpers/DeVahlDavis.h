#ifndef HYBRID_SCATTERED_UNIFORM_HEATEDCYLINDER_H
#define HYBRID_SCATTERED_UNIFORM_DEVAHLDAVIS_H

#include <medusa/Medusa.hpp>
#include "Convection.hpp"
#include "math_helper.hpp"

using namespace mm;

template <int dim>
class DVD : public Convection<dim> {
    using Convection<dim>::Convection;

  protected:
    typedef typename Convection<dim>::scal_t scal_t;
    typedef typename Convection<dim>::vec_t vec_t;

    void constructDomain(DomainDiscretization<vec_t>& domain) override {
        if (this->verbosity_ > 1) {
            cout << "Domain build started." << endl;
        }

        if (this->xml_in_.template get<bool>("problem.quads")) {
            constructQuadsDomain(domain);
        } else {
            constructSplitDomain(domain);
        }
    }

    void constructQuadsDomain(DomainDiscretization<vec_t>& domain) {
        BoxShape<vec_t> box(0.0, 1.0);

        int scattered_type = this->RBFFD + 10;
        auto seed = this->xml_in_.template get<int>("domain.seed");
        GeneralFill<vec_t> gf;
        gf.seed(seed).numSamples(this->xml_in_.template get<int>("domain.num_samples"));

        if (this->xml_in_.template get<string>("domain.type") == "scattered") {
            domain = box.discretizeBoundaryWithStep(this->h_);
            domain.fill(gf, this->h_, scattered_type);

            // Relax.
            if (this->xml_in_.template get<bool>("relax.enabled")) {
                BasicRelax relax;
                relax.initialHeat(this->xml_in_.template get<scal_t>("relax.initial_heat"))
                    .finalHeat(this->xml_in_.template get<scal_t>("relax.final_heat"))
                    .iterations(this->xml_in_.template get<int>("relax.iter_num"))
                    .numNeighbours(this->xml_in_.template get<int>("relax.num_neighbours"))
                    .projectionType(BasicRelax::DO_NOT_PROJECT);

                // Relax.
                relax(domain);
            }
        } else if (this->xml_in_.template get<string>("domain.type") == "uniform") {
            // Box domain.
            domain = box.discretizeWithStep(this->h_);
        } else if (this->xml_in_.template get<string>("domain.type") == "hybrid") {
            // Box domain.
            domain = box.discretizeWithStep(this->h_);

            Range<int> nodes_to_remove;
            for (int j : domain.interior()) {
                if ((((domain.pos(j, 0) < 0.5) && (domain.pos(j, 1) > 0.5)) ||
                     ((domain.pos(j, 0) > 0.5) && (domain.pos(j, 1) < 0.5)))) {
                    nodes_to_remove.push_back(j);
                }
            }
            domain.removeNodes(nodes_to_remove);
            // Fill empty space.
            domain.fill(gf, this->h_, scattered_type);
        } else {
            assert_msg(false, "Unknown discretization type.");
        }

        if (this->xml_in_.template get<bool>("domain.no_corner")) {
            removeCornerNodes();
        }

        // Assign node type scattered/uniform.
        int n = this->support_size_collocation_;
        domain.findSupport(FindClosest(n).forNodes(domain.interior()));
        domain.findSupport(
            FindClosest(n).forNodes(domain.boundary()).searchAmong(domain.interior()).forceSelf());
        this->engine_indices_.resize(this->engine_count);
        for (int j : domain.all()) {
            // Check id scattered in support.
            auto support = domain.support(j);
            bool scattered_inside = false;
            for (int i : support) {
                scattered_inside = scattered_inside or (domain.type(i) == scattered_type);
            }

            // Centre of mass.
            vec_t cms;
            cms.setZero();
            vec_t r_0 = domain.pos(j);
            for (int i = 1; i < support.size(); ++i) {
                cms += domain.pos(support[i]) - r_0;
            }

            if (scattered_inside or (cms.norm() > EPS and domain.type(j) > 0)) {
                // Use RBF-FD if at least one node from stencil is scattered.
                this->engine_indices_[this->RBFFD].push_back(j);
            } else {
                if ((((domain.pos(j, 0) < 0.5) && (domain.pos(j, 1) < 0.5)) ||
                     ((domain.pos(j, 0) > 0.5) && (domain.pos(j, 1) > 0.5))) and
                    (domain.type(j) < 0)) {
                    this->engine_indices_[this->SUPERCOL].push_back(j);
                } else {
                    this->engine_indices_[this->COLLOCATION].push_back(j);
                }
            }
        }

        // Boundary maps.
        if (this->verbosity_ > 1) {
            cout << "Determining boundary types.." << endl;
        }
        determineBoundaryTypes();
        this->interior_ = this->domain_.interior();

        if (this->verbosity_ > 1) {
            cout << "Domain build finished." << endl;
        }

        // Ghost nodes.
        Range<int> node_list;
        if (this->xml_in_.template get<string>("domain.type") == "hybrid") {
            for (int i : domain.boundary()) {
                vec_t pos = domain.pos(i);
                double x = pos[0];
                double y = pos[1];

                if (x <= 0.5 and y >= 0.5) {
                    node_list.push_back(i);
                } else if (x >= 0.5 and y <= 0.5) {
                    node_list.push_back(i);
                }
            }
        } else if (this->xml_in_.template get<string>("domain.type") == "scattered") {
            node_list = domain.boundary();
        }
        //        this->ghost_mapping_ = domain.addGhostNodes(this->h_, 0, node_list);
    }

    void constructSplitDomain(DomainDiscretization<vec_t>& domain) {
        BoxShape<vec_t> box(0.0, 1.0);

        int scattered_type = this->RBFFD + 10;
        auto seed = this->xml_in_.template get<int>("domain.seed");
        GeneralFill<vec_t> gf;
        gf.seed(seed).numSamples(this->xml_in_.template get<int>("domain.num_samples"));
        double x_threshold = -1, y_threshold = -1;
        if (this->xml_in_.template get<string>("domain.type") == "scattered") {
            domain = box.discretizeBoundaryWithStep(this->h_);
            domain.fill(gf, this->h_, scattered_type);
        } else if (this->xml_in_.template get<string>("domain.type") == "uniform") {
            // Box domain.
            domain = box.discretizeWithStep(this->h_);
        } else if (this->xml_in_.template get<string>("domain.type") == "hybrid") {
            // Box domain.
            domain = box.discretizeWithStep(this->h_);

            Range<int> nodes_to_remove;
            if (this->xml_in_.template get<string>("domain.split") == "horizontal") {
                y_threshold = box.bbox().first[1] +
                              this->xml_in_.template get<double>("domain.scattered_h") * this->h_;
            } else {
                x_threshold = box.bbox().first[0] +
                              this->xml_in_.template get<double>("domain.scattered_h") * this->h_;
            }
            for (int j : domain.interior()) {
                if ((domain.pos(j, 0) < x_threshold) or (domain.pos(j, 1) < y_threshold)) {
                    nodes_to_remove.push_back(j);
                }
            }
            domain.removeNodes(nodes_to_remove);
            // Fill empty space.
            domain.fill(gf, this->h_, scattered_type);
        } else {
            assert_msg(false, "Unknown discretization type.");
        }

        if (this->xml_in_.template get<bool>("domain.no_corner")) {
            removeCornerNodes();
        }

        // Assign node type scattered/uniform.
        int n = this->support_size_collocation_;
        domain.findSupport(FindClosest(n).forNodes(domain.interior()));
        domain.findSupport(
            FindClosest(n).forNodes(domain.boundary()).searchAmong(domain.interior()).forceSelf());
        this->engine_indices_.resize(this->engine_count);
        for (int j : domain.all()) {
            // Check id scattered in support.
            auto support = domain.support(j);
            bool scattered_inside = false;
            for (int i : support) {
                scattered_inside = scattered_inside or (domain.type(i) == scattered_type);
            }

            // Centre of mass.
            vec_t cms;
            cms.setZero();
            vec_t r_0 = domain.pos(j);
            for (int i = 1; i < support.size(); ++i) {
                cms += domain.pos(support[i]) - r_0;
            }

            if (scattered_inside or (cms.norm() > EPS and domain.type(j) > 0)) {
                // Use RBF-FD if at least one node from stencil is scattered.
                this->engine_indices_[this->RBFFD].push_back(j);
            } else {
                if (((domain.pos(j, 0) < x_threshold) or (domain.pos(j, 1) < y_threshold)) and
                    domain.type(j) < 0) {  // TODO(mjancic): if is not OK, needs to be fixed!!!!
                    this->engine_indices_[this->SUPERCOL].push_back(j);
                } else {
                    this->engine_indices_[this->COLLOCATION].push_back(j);
                }
            }
        }

        // Boundary maps.
        if (this->verbosity_ > 1) {
            cout << "Determining boundary types.." << endl;
        }
        determineBoundaryTypes();
        this->interior_ = this->domain_.interior();

        if (this->verbosity_ > 1) {
            cout << "Domain build finished." << endl;
        }

        // Ghost nodes.
        Range<int> node_list;
        if (this->xml_in_.template get<string>("domain.type") == "hybrid") {
            for (int i : domain.boundary()) {
                vec_t pos = domain.pos(i);
                double x = pos[0];
                double y = pos[1];

                if (x <= 0.5 and y >= 0.5) {
                    node_list.push_back(i);
                } else if (x >= 0.5 and y <= 0.5) {
                    node_list.push_back(i);
                }
            }
        } else if (this->xml_in_.template get<string>("domain.type") == "scattered") {
            node_list = domain.boundary();
        }
        //        this->ghost_mapping_ = domain.addGhostNodes(this->h_, 0, node_list);
    }

    void removeCornerNodes() {
        auto borders = this->domain_.shape().bbox();
        Range<int> corner = this->domain_.positions().filter([&](const vec_t& p) {
            // remove nodes that are EPS close to more than 1 border
            return (((p - borders.first) < EPS).size() + ((borders.second - p) < EPS).size()) >
                   1;
        });

        this->domain_.removeNodes(corner);
    }

    void determineBoundaryTypes() {
        auto borders = this->domain_.shape().bbox();
        auto closestOther = [&](const vec_t& p) {
            return std::min((p - borders.first)(Eigen::lastN(dim - 1)).minCoeff(),
                            (borders.second - p)(Eigen::lastN(dim - 1)).minCoeff());
        };

        Range<int> left_idx = this->domain_.positions().filter([&](const vec_t& p) {
            return (p[0] - borders.first[0] < EPS) && closestOther(p) > EPS;
        });
        Range<int> right_idx = this->domain_.positions().filter([&](const vec_t& p) {
            return (borders.second[0] - p[0] < EPS) && closestOther(p) > EPS;
        });
        Range<int> boundary = this->domain_.boundary();
        Range<int> other_idx = boundary.filter([&](const int idx) {
            for (auto i : left_idx)
                if (idx == i) return false;
            for (auto i : right_idx)
                if (idx == i) return false;
            return true;
        });

        this->edge_indices_.resize(this->type_count);
        this->edge_indices_[this->NEUTRAL] = boundary[other_idx];
        this->edge_indices_[this->COLD] = left_idx;
        this->edge_indices_[this->HOT] = right_idx;
    }
};

#endif  // HYBRID_SCATTERED_UNIFORM_HEATEDCYLINDER_H
