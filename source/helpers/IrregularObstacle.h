#ifndef HYBRID_SCATTERED_UNIFORM_IRREGULAR_clover_H
#define HYBRID_SCATTERED_UNIFORM_IRREGULAR_clover_H

#include "medusa/Medusa.hpp"
#include "Convection.hpp"
#include "math_helper.hpp"
#include "CloverShape.hpp"
#include "HybridFill.hpp"
#include "DiscretizationHelper.hpp"

using namespace mm;

template <typename vec_t>
class IrregularObstacle : public Convection<vec_t::dim> {
    using Convection<vec_t::dim>::Convection;

  protected:
    typedef typename Convection<vec_t::dim>::scal_t scal_t;

    void constructDomain(DomainDiscretization<vec_t>& domain) override {
        if (this->verbosity_ > 1) {
            cout << "Domain build started." << endl;
        }
        double domain_size = this->xml_in_.template get<double>("domain.size");
        string discretization_type = this->xml_in_.template get<string>("domain.type");
        int hot_type = this->xml_in_.template get<int>("domain.hot_type");
        int cold_type = this->xml_in_.template get<int>("domain.cold_type");
        double h_small = this->xml_in_.template get<double>("num.sphere_h");
        double h_large = this->xml_in_.template get<double>("num.h");
        int obstacle_count = this->xml_in_.template get<int>("domain.clover_count");
        double max_obstacle_resize =
            this->xml_in_.template get<double>("domain.max_obstacle_scale");
        double min_obstacle_resize =
            this->xml_in_.template get<double>("domain.min_obstacle_scale");
        std::mt19937 gen(this->xml_in_.template get<int>("domain.generate_seed"));
        double delta_h = this->xml_in_.template get<double>("domain.scattered_h");
        int scattered_type = this->RBFFD + 1;
        int collocation_type = this->COLLOCATION + 1;
        int super_collocation_type = this->SUPERCOL + 1;

        // Box.
        vec_t beg = vec_t::Zero();
        vec_t end = vec_t::Constant(domain_size);
        BoxShape<vec_t> box(beg, end);

        // Random obstacle size, rotation and position.
        std::uniform_real_distribution<double> size_distribution{min_obstacle_resize,
                                                                 max_obstacle_resize};
        std::uniform_real_distribution<double> rotations_distribution{-mm::PI, mm::PI};
        std::uniform_real_distribution<double> coord_distribution{0.1, 0.9};

        // Positions obstacles in box.
        Range<TranslatedShape<vec_t>> clovers_hot, clovers_cold;
        for (int m = 0; m < obstacle_count; ++m) {
            // Generate obstacles of equal size.
            double obstacle_size = size_distribution(gen);
            auto candidate =
                CloverShape<vec_t>{obstacle_size, this->xml_in_.template get<int>("domain.seed")};

            // Generate hot and cold obstacle.
            for (int i = 0; i < 2; ++i) {
                auto [lo, hi] = candidate.bbox();
                int iter = 0;
                do {
                    // Randomly rotate obstacle.
                    Eigen::MatrixXd rotation_mat;
                    if constexpr (vec_t::dim == 3) {
                        // DIM == 3
                        Eigen::AngleAxisd rollAngle(rotations_distribution(gen),
                                                    Eigen::Vector3d::UnitZ());
                        Eigen::AngleAxisd yawAngle(rotations_distribution(gen),
                                                   Eigen::Vector3d::UnitY());
                        Eigen::AngleAxisd pitchAngle(rotations_distribution(gen),
                                                     Eigen::Vector3d::UnitX());

                        Eigen::Quaternion<double> q = rollAngle * yawAngle * pitchAngle;
                        rotation_mat = q.matrix();
                    } else {
                        // DIM == 2
                        rotation_mat = Eigen::Rotation2D<double>(rotations_distribution(gen))
                                           .toRotationMatrix();
                    }
                    // Randomly reposition the obstacle.
                    vec_t obstacle_ll_corner;
                    // Get random translation.
                    for (int j = 0; j < vec_t::dim; ++j) {
                        obstacle_ll_corner[j] = coord_distribution(gen);
                    }
                    prn(obstacle_ll_corner);

                    // Check inside. Discretize and check.
                    auto clover_candidate =
                        candidate.rotate(rotation_mat).translate(obstacle_ll_corner - lo);
                    auto d_clover = clover_candidate.discretizeBoundaryWithStep(h_small * 0.1);
                    auto shape = BoxShape<vec_t>(0.1, 0.9) -
                                 BoxShape<vec_t>(10, 10.1);  // Box 0.1 to 0.9 is taken to prevent
                                                             // obstacles from being close to bnd.
                    for (auto clover : clovers_hot + clovers_cold) {
                        shape = shape - clover;
                    }
                    bool is_inside = true;
                    for (auto p : d_clover.positions()) {
                        is_inside = shape.contains(p);
                        if (not(is_inside)) {
                            break;
                        }
                    }

                    // Add obstacle to domain.
                    if (is_inside) {
                        (i == 0) ? clovers_hot.push_back(clover_candidate)
                                 : clovers_cold.push_back(clover_candidate);
                        break;
                    }
                } while (iter++ < 1000);
            }
        }
        auto hot = clovers_hot[0] + clovers_hot[1];
        auto cold = clovers_cold[0] + clovers_cold[1];

        // DISCRETIZE.
        // Due to issues with surface fill, the internodal distance on the clover is multiplied by
        // factor 0.9, to assure fill doesn't position nodes too close to the boundary.
        auto d_hot = hot.discretizeBoundaryWithDensity([=](vec_t p) { return h_small; }, hot_type);
        auto d_cold =
            cold.discretizeBoundaryWithDensity([=](vec_t p) { return h_small; }, cold_type);
        mm::KDTreeMutable<vec_t> internal_search(d_hot.positions() + d_cold.positions());

        if constexpr (vec_t::dim == 2) {
            mm::KDTreeMutable<vec_t> hot_search(d_hot.positions());
            for (int i = 0; i < d_hot.size(); ++i) {
                vec_t p = d_hot.pos(i);
                Range<double> distances2;
                Range<int> closest;
                std::tie(closest, distances2) = hot_search.query(p, 3);
                if (distances2[2] > mm::ipow<2>(1.95 * h_small)) {
                    vec_t new_p = (d_hot.pos(closest[0]) + d_hot.pos(closest[2])) / 2;
                    vec_t new_n = (d_hot.normal(closest[0]) + d_hot.normal(closest[2])) / 2;
                    d_hot.addBoundaryNode(new_p, d_hot.type(closest[0]), new_n.normalized());
                    hot_search.insert(new_p);
                    std::cout << "added node on clover : " << new_p << std::endl;
                }
            }
            mm::KDTreeMutable<vec_t> cold_search(d_cold.positions());
            for (int i = 0; i < d_cold.size(); ++i) {
                vec_t p = d_cold.pos(i);
                Range<double> distances2;
                Range<int> closest;
                std::tie(closest, distances2) = cold_search.query(p, 3);
                if (distances2[2] > mm::ipow<2>(1.95 * h_small)) {
                    vec_t new_p = (d_cold.pos(closest[0]) + d_cold.pos(closest[2])) / 2;
                    vec_t new_n = (d_cold.normal(closest[0]) + d_cold.normal(closest[2])) / 2;
                    d_cold.addBoundaryNode(new_p, d_cold.type(closest[0]), new_n.normalized());
                    cold_search.insert(new_p);
                    std::cout << "added node on clover : " << new_p << std::endl;
                }
            }
        }

        // Delta_h width.
        double scatter_dist = h_large * delta_h;
        // Function for uniform nodes.
        auto grid_f = [&](vec_t& p) {
            if (discretization_type == "scattered") {
                return false;
            } else if (discretization_type == "hybrid") {
                return internal_search.query(p).second[0] > scatter_dist * scatter_dist;
            }
            return true;
        };
        // Nodal density.
        auto var_h = [&](vec_t p) {
            double dist = std::sqrt(internal_search.query(p).second[0]);
            if (discretization_type == "fdm") {
                return h_large;
            } else {
                return std::min(h_large, h_small + (h_large - h_small) * dist / scatter_dist);
            };
        };
        // Fill domain with nodes.
        HybridFill<vec_t> hybrid_fill;
        hybrid_fill.seed(this->xml_in_.template get<int>("domain.seed"));
        domain = variableBox<vec_t>(beg, end, var_h, grid_f, -scattered_type, -collocation_type);
        domain -= d_cold;
        domain -= d_hot;

        if (discretization_type == "fdm") {
            domain.removeNodes(domain.types() <= cold_type);
        }

        // Fill domain.
        hybrid_fill(domain, var_h, grid_f, scattered_type, collocation_type);
        prn(domain.size());
        // TODO: There is something wrong with normals. We thus supersample clovers and fix the
        // normals.
        auto super_d_hot =
            hot.discretizeBoundaryWithDensity([=](vec_t p) { return h_small * 0.01; }, hot_type);
        auto super_d_cold =
            cold.discretizeBoundaryWithDensity([=](vec_t p) { return h_small * 0.01; }, cold_type);
        KDTree<vec_t> hot_tree(super_d_hot.positions());
        KDTree<vec_t> cold_tree(super_d_cold.positions());

        // IF FDM: handle with care.
        if (discretization_type == "fdm") {
            domain.removeNodes(domain.types() <= cold_type);
            // Assign boundary.
            Range<int> indexes_to_remove;
            Range<vec_t> nodes_hot, nodes_cold;
            Range<vec_t> clover_normals_cold, clover_normals_hot;
            Range<double> distances2, distances2_c;
            Range<int> closest, closest_c;
            domain.findSupport(FindClosest(2 * vec_t::dim + 1));
            for (int i : domain.interior()) {
                auto p = domain.pos(i);
                Range<vec_t> support = domain.supportNodes(i);
                domain.support(i).clear();
                if ((support[1] - p).squaredNorm() * 1.01 < (support.back() - p).squaredNorm()) {
                    indexes_to_remove.push_back(i);
                    if ((support[1] - p).squaredNorm() * 1.01 < (support[2] - p).squaredNorm()) {
                        continue;  // Remove isolated nodes.
                    }
                    std::tie(closest, distances2) = hot_tree.query(p, var_h(p));
                    std::tie(closest_c, distances2_c) = cold_tree.query(p, var_h(p));
                    if (closest.empty() and !closest_c.empty()) {
                        clover_normals_cold.push_back(super_d_cold.normal(closest_c[0]));
                        nodes_cold.push_back(p);
                    } else if (!closest.empty() and closest_c.empty()) {
                        clover_normals_hot.push_back(super_d_hot.normal(closest[0]));
                        nodes_hot.push_back(p);
                    } else {
                        if (*min_element(distances2_c.begin(), distances2_c.end()) >
                            *min_element(distances2.begin(), distances2.end())) {
                            clover_normals_hot.push_back(super_d_hot.normal(closest[0]));
                            nodes_hot.push_back(p);
                        } else {
                            clover_normals_cold.push_back(super_d_cold.normal(closest_c[0]));
                            nodes_cold.push_back(p);
                        }
                    }
                }
            }
            domain.removeNodes(indexes_to_remove);

            for (int i = 0; i < nodes_hot.size(); ++i) {
                domain.addBoundaryNode(nodes_hot[i], hot_type, -clover_normals_hot[i]);
            }
            for (int i = 0; i < nodes_cold.size(); ++i) {
                domain.addBoundaryNode(nodes_cold[i], cold_type, -clover_normals_cold[i]);
            }

            domain.findSupport(FindClosest(2 * vec_t::dim + 1).forNodes(domain.interior()));
            indexes_to_remove.clear();
            for (int i : domain.interior()) {
                auto p = domain.pos(i);
                Range<vec_t> support = domain.supportNodes(i);
                scal_t d_0 = (support[1] - p).squaredNorm() * 1.01;
                if (d_0 < (support.back() - p).squaredNorm()) {
                    indexes_to_remove.push_back(i);
                    vec_t n = vec_t::Zero();
                    int nb_idx = 1;
                    int type = 0;
                    while (d_0 > (support[nb_idx] - p).squaredNorm()) {
                        if (domain.type(domain.support(i, nb_idx)) < 0) {
                            n += domain.normal(domain.support(i, nb_idx));
                            if (type == 0) type = domain.type(domain.support(i, nb_idx));
                        }
                        ++nb_idx;
                    }
                    domain.addBoundaryNode(p, type, n.normalized());
                }
                domain.support(i).clear();
            }
            domain.removeNodes(indexes_to_remove);
        }

        if (this->xml_in_.template get<bool>("domain.no_corner")) {
            if (this->verbosity_ > 1) {
                cout << "Removing corners." << endl;
            }
            removeCornerNodes();
        }

        auto borders = domain.shape().bbox();
        scal_t d = h_small / 2;
        Range<int> closeToBorder;
        for (int i : domain.interior()) {
            vec_t p = domain.pos(i);
            bool close_to_box =
                (((p - borders.first) < d).size() + ((borders.second - p) < d).size()) > 0;
            bool close_to_obstacles = std::min(hot_tree.query(p).second[0],
                                               cold_tree.query(p).second[0]) < mm::ipow<2>(d);
            if (close_to_box || close_to_obstacles) closeToBorder.push_back(i);
        }
        domain.removeNodes(closeToBorder);
        prn(closeToBorder);

        // Assign node approximation.
        if (this->verbosity_ > 1) {
            std::cout << "Assigning approx engine." << std::endl;
        }
        assignApproximationEngine(domain, discretization_type, scattered_type, cold_type, hot_type);

        // Boundary maps.
        if (this->verbosity_ > 1) {
            cout << "Determining boundary types.." << endl;
        }
        determineBoundaryTypes();
        this->interior_ = this->domain_.interior();

        if (this->verbosity_ > 1) {
            cout << "Domain build finished." << endl;
        }
    }

    void removeCornerNodes() {
        auto borders = this->domain_.shape().bbox();
        Range<int> corner = this->domain_.positions().filter([&](const vec_t& p) {
            // remove nodes that are EPS close to more than 1 border
            return (((p - borders.first).cwiseAbs() < EPS).size() +
                    ((borders.second - p).cwiseAbs() < EPS).size()) > 1;
        });
        this->domain_.removeNodes(corner);
    }

    void assignApproximationEngine(DomainDiscretization<vec_t>& domain, string discretization_type,
                                   int scattered_type, int cold_type, int hot_type) {
        this->engine_indices_.resize(this->engine_count);
        if (discretization_type == "scattered") {
            this->engine_indices_[this->RBFFD] = domain.all();
        } else if (discretization_type == "fdm") {
            // FDM-like.
            for (int j = 0; j < domain.size(); ++j) {
                if (domain.type(j) < 0) {
                    this->engine_indices_[this->SUPERCOL].push_back(j);
                } else {
                    this->engine_indices_[this->COLLOCATION].push_back(j);
                }
            }
        } else {
            domain.findSupport(
                FindClosest(this->support_size_collocation_).forNodes(domain.interior()));
            domain.findSupport(
                FindClosest(this->support_size_supercol_).forNodes(domain.boundary()));

            for (int j = 0; j < domain.size(); ++j) {
                // Check if any scattered type in support.
                auto support = domain.support(j);
                auto support_idxs = (domain.types()[support]).asRange().filter([&](int i) {
                    return i == scattered_type || i == -scattered_type || i == cold_type ||
                           i == hot_type;
                });
                bool scattered = !support_idxs.empty();
                domain.support(j).clear();

                if (domain.type(j) > 0 &&
                    ((domain.pos(support[1]) - domain.pos(j)).squaredNorm() * 1.01 <
                     (domain.pos(support.back()) - domain.pos(j)).squaredNorm())) {
                    scattered = true;
                }
                // Hybrid.
                if (scattered) {
                    this->engine_indices_[this->RBFFD].push_back(j);
                } else if (domain.type(j) < 0) {
                    this->engine_indices_[this->SUPERCOL].push_back(j);
                } else {
                    this->engine_indices_[this->COLLOCATION].push_back(j);
                }
            }
        }
    }

    void determineBoundaryTypes() {
        if constexpr (vec_t::dim == 3) {
            auto left = this->domain_.types() == -1;
            auto right = this->domain_.types() == -2;
            auto front = this->domain_.types() == -3;
            auto back = this->domain_.types() == -4;
            auto top = this->domain_.types() == -6;
            auto bottom = this->domain_.types() == -5;
            int cold_type = this->xml_in_.template get<int>("domain.cold_type");
            int hot_type = this->xml_in_.template get<int>("domain.hot_type");
            auto cold_spheres = this->domain_.types() == cold_type;
            auto hot_spheres = this->domain_.types() == hot_type;

            this->edge_indices_.resize(this->type_count);
            this->edge_indices_[this->NEUTRAL] = top + bottom + left + right + front + back;
            this->edge_indices_[this->COLD] = cold_spheres;
            this->edge_indices_[this->HOT] = hot_spheres;
        } else if (vec_t::dim == 2) {
            auto left = this->domain_.types() == -1;
            auto right = this->domain_.types() == -2;
            auto top = this->domain_.types() == -4;
            auto bottom = this->domain_.types() == -3;
            int cold_type = this->xml_in_.template get<int>("domain.cold_type");
            int hot_type = this->xml_in_.template get<int>("domain.hot_type");
            auto cold_spheres = this->domain_.types() == cold_type;
            auto hot_spheres = this->domain_.types() == hot_type;

            this->edge_indices_.resize(this->type_count);
            this->edge_indices_[this->NEUTRAL] = top + bottom + left + right;
            this->edge_indices_[this->COLD] = cold_spheres;
            this->edge_indices_[this->HOT] = hot_spheres;
        }
    }
};

#endif  // HYBRID_SCATTERED_UNIFORM_IRREGULAR_H
