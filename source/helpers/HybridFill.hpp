#ifndef MEDUSA_BITS_DOMAINS_HYBRIDFILL_HPP_
#define MEDUSA_BITS_DOMAINS_HYBRIDFILL_HPP_

/**
 * @file
 * Implementation of the general node placing algorithm.
 */

#include "HybridFill_fwd.hpp"
#include "medusa/bits/domains/discretization_helpers.hpp"

#include <medusa/bits/utils/assert.hpp>
#include <medusa/bits/types/Range.hpp>
#include <random>
#include <utility>
#include <medusa/bits/utils/randutils.hpp>
#include <medusa/bits/spatial_search/KDTreeMutable.hpp>
#include <medusa/bits/spatial_search/KDTree.hpp>

namespace mm {

template <typename vec_t>
HybridFill<vec_t>::HybridFill() : seed_(get_seed()) {}

/// @cond
template <typename vec_t>
template <typename h_func_t, typename f_func_t>
void HybridFill<vec_t>::operator()(DomainDiscretization<vec_t>& domain, const h_func_t& h,
                                   const f_func_t& fill_with_grid, int scat_type, int grid_type) {
    KDTreeMutable<vec_t> tree;
    operator()(domain, h, fill_with_grid, tree, scat_type, grid_type);
}

template <typename vec_t>
template <typename h_func_t, typename f_func_t, typename search_structure_t>
void HybridFill<vec_t>::operator()(DomainDiscretization<vec_t>& domain, const h_func_t& h,
                                   const f_func_t& fill_with_grid, search_structure_t& search,
                                   int scat_type, int grid_type) {
    KDTree<vec_t> contains_search;
    domain.makeDiscreteContainsStructure(contains_search);
    auto contains_function = [&](const vec_t point) {
        return domain.contains(point, contains_search);
    };
    operator()(domain, h, fill_with_grid, search, contains_function, scat_type, grid_type);
}

template <typename vec_t>
template <typename h_func_t, typename f_func_t, typename search_structure_t,
          typename contains_function_t>
void HybridFill<vec_t>::operator()(DomainDiscretization<vec_t>& domain, const h_func_t& h,
                                   const f_func_t& fill_with_grid, search_structure_t& search,
                                   contains_function_t& contains_function, int scat_type,
                                   int grid_type) {
    std::mt19937 gen(seed_);

    int cur_node = 0;
    int end_node = domain.size();
    if (end_node == 0) {  // If domain is empty, pick a random node inside it.
        vec_t lo_bound, hi_bound, random_node;
        std::tie(lo_bound, hi_bound) = domain.shape().bbox();
        std::vector<std::uniform_real_distribution<scalar_t>> distributions;
        for (int j = 0; j < dim; ++j) distributions.emplace_back(lo_bound[j], hi_bound[j]);
        int count = 0;
        do {
            for (int j = 0; j < dim; ++j) {
                random_node[j] = distributions[j](gen);
            }
            if (++count >= 10000) {
                std::string message =
                    "No suitable node in domain could be found after 10000 tries."
                    " This might happen if domain volume is very small compared "
                    "to the volume of the bounding box. Try manually supplying "
                    "the initial point.";
                throw std::runtime_error(message);
            }
        } while (!contains_function(random_node));
        domain.addInternalNode(random_node, fill_with_grid(random_node) ? grid_type : scat_type);
        end_node = 1;
    }

    // Main algorithm loop.
    search.insert(domain.positions());
    Range<int> to_remove;
    KDTreeMutable<vec_t> grid_search;
    grid_search.insert(domain.positions());
//    if (domain.boundary().size() > 0) {
//        grid_search.insert(domain.positions()[domain.boundary()]);  // Boundary nodes should not be
//    } // removed due to grid interference

    std::queue<int> grid_edge_nodes;
    std::queue<std::pair<int, int>> grid_skip_indices;
    int skip_start = -1, skip_end = -1;
    while (cur_node < end_node && end_node < max_points) {
        if (skip_start < 0 && !grid_skip_indices.empty()) {
            std::tie(skip_start, skip_end) = grid_skip_indices.front();
            grid_skip_indices.pop();
        }  // This assumes that two grid areas do not directly follow eachother. Reconsider.
        if (skip_start >= 0 && skip_start <= cur_node) {
            if (!grid_edge_nodes.empty() && grid_edge_nodes.front() < skip_end) {
                cur_node = grid_edge_nodes.front();
                grid_edge_nodes.pop();
            } else {
                if (skip_end >= end_node) break;
                cur_node = skip_end;
                skip_start = -1;
            }
        }
        vec_t p = domain.pos(cur_node);
        scalar_t r = h(p);
        assert_msg(r > 0, "Nodal spacing radius should be > 0, got %g.", r);

        std::vector<vec_t, Eigen::aligned_allocator<vec_t>> candidates;
        // TODO: Implement with less copying
        if (domain.type(cur_node) < 0 && fill_with_grid(p)) {
            candidates = gridCandidates(r);
        } else {
            candidates = discretization_helpers::SphereDiscretization<scalar_t, dim>::construct(
                r, n_samples, gen);
        }
        // filter candidates regarding the domain and proximity criteria
        for (const auto& f : candidates) {
            vec_t node = p + f;                      // Shift center to point `p`.
            if (!contains_function(node)) continue;  // If node is not in the domain.
            if (search.existsPointInSphere(node, r * zeta)) continue;  // If node is not far enough
            domain.addInternalNode(node, scat_type);                   // from other nodes.
            search.insert(node);
            end_node++;

            // Issues:
            //         - Unified type/h function or separate calls?

            if (fill_with_grid(node)) {
                domain.type(end_node - 1) = grid_type;
                placeGrid(domain, end_node, h(node), fill_with_grid, search, grid_search,
                          contains_function, grid_edge_nodes, grid_skip_indices, to_remove,
                          grid_type);
            }
        }
        cur_node++;
    }
    // TODO: Node removal or new node construction from tree?
    domain.removeNodes(to_remove);
    if (end_node >= max_points) {
        std::cerr << "Maximum number of points reached, fill may be incomplete." << std::endl;
    }
}

// TODO: Refactor this mess
template <typename vec_t>
template <typename func_t, typename search_structure_t, typename contains_function_t>
void HybridFill<vec_t>::placeGrid(DomainDiscretization<vec_t>& domain, int& end_node, scalar_t r,
                                  const func_t& is_grid, search_structure_t& search,
                                  search_structure_t& grid_search,
                                  contains_function_t& contains_function,
                                  std::queue<int>& grid_edge,
                                  std::queue<std::pair<int, int>>& grid_skip, Range<int>& to_remove,
                                  int grid_type) {
    int first_grid = end_node - 1;
    int cur_node = first_grid;
    grid_search.insert(domain.pos(cur_node));
    while (cur_node < end_node) {
        vec_t cur_pos = domain.pos(cur_node);
        // TODO: Improve and implement for N-d (something should already exist in the helper
        // functions)
        auto candidates = gridCandidates(r);
        bool is_edge = false;
        for (const auto& cand_offset : candidates) {
            vec_t cand_pos = cur_pos + cand_offset;      // Shift center to point `p`.
            if (!contains_function(cand_pos)) continue;  // If node is not in the domain.
            if (!is_grid(cand_pos)) {
                if (!is_edge) {
                    grid_edge.push(cur_node);
                    is_edge = true;
                }
                continue;
            }
            if (search.existsPointInSphere(cand_pos, r * zeta)) {
                if (grid_search.existsPointInSphere(cand_pos, r * zeta))
                    continue;
                else {
                    while (search.existsPointInSphere(cand_pos, r * zeta)) {
                        int i = search.query(cand_pos).first[0];
                        search.remove(i);
                        to_remove.push_back(i);
                    }
                }
            }
            domain.addInternalNode(cand_pos, grid_type);
            search.insert(cand_pos);
            grid_search.insert(cand_pos);
            end_node++;
        }
        cur_node++;
    }
    grid_skip.template emplace(first_grid, end_node);
}

template <typename vec_t>
std::vector<vec_t, Eigen::aligned_allocator<vec_t>> HybridFill<vec_t>::gridCandidates(scalar_t r) {
    if (grid_candidates_.empty() || grid_candidates_[0](0) != r) {
        bool append = grid_candidates_.empty();
        int i = 0;
        for (int d = 0; d < dim; ++d) {
            for (scalar_t value : {r, -r}) {
                if (append) {
                    grid_candidates_.push_back(vec_t::Zero());
                }
                grid_candidates_[i](d) = value;
                ++i;
            }
        }
    }
    return grid_candidates_;
}

template <typename vec_t>
HybridFill<vec_t>& HybridFill<vec_t>::proximityTolerance(scalar_t zeta) {
    assert_msg((0 < zeta && zeta < 1), "Zeta must be between 0 and 1, got %f.", zeta);
    this->zeta = zeta;
    return *this;
}
/// @endcond

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_HYBRIDFILL_HPP_
