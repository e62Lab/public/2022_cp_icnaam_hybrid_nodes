#ifndef ICCS_HYBRID_NODES_DISCRETIZATIONHELPER_HPP
#define ICCS_HYBRID_NODES_DISCRETIZATIONHELPER_HPP
#include "medusa/Medusa.hpp"
#include "CloverShape.hpp"
#include "HybridFill.hpp"

using namespace mm;

template <typename vec_t>
mm::Range<vec_t> cornerPositions(vec_t beg, vec_t end) {
    mm::Range<vec_t> corners;
    for (unsigned perm = 0; perm < (1 << vec_t::dim); ++perm) {
        vec_t c;
        for (int d = 0; d < vec_t::dim; ++d) {
            if (perm & (1 << d)) {
                c[d] = end[d];
            } else {
                c[d] = beg[d];
            }
        }
        corners.push_back(c);
    }
    return corners;
}
template <typename vec_t>
mm::Range<vec_t> discretizeLineWithDensity(
    vec_t p, vec_t q, const std::function<typename vec_t::scalar_t(vec_t&)> h) {
    typedef typename vec_t::scalar_t scal_t;
    scal_t dist = (q - p).norm();
    assert_msg(dist >= 1e-15, "Points %s and %s are equal.", p, q);
    vec_t dir = (q - p) / dist;
    scal_t cur_dist = 0;
    int sign = 0;
    mm::Range<vec_t> points = {p, q};
    scal_t step;
    bool trip = false;
    while (true) {
        if (sign % 2 == 0) {
            step = h(p);
            if (cur_dist + step < dist) {
                cur_dist += step;
                p += step * dir;
                points.push_back(p);
            } else {
                if (trip) break;
                trip = true;
            }
        } else {
            step = h(q);
            if (cur_dist + step < dist) {
                cur_dist += step;
                q -= step * dir;
                points.push_back(q);
            } else {
                if (trip) break;
                trip = true;
            }
        }
        ++sign;
    }
    scal_t e_as_is = std::abs(1 - (dist - cur_dist) / step);
    scal_t e_remove = std::abs(1 - (dist - cur_dist + step) / step);
    if (e_as_is > e_remove) {
        points.pop_back();
    }
    return points;
}
template <typename vec_t>
mm::Vec<typename vec_t::scalar_t, vec_t::dim - 1> projectVecDown(vec_t v, int lost_dim) {
    mm::Vec<typename vec_t::scalar_t, vec_t::dim - 1> projected;
    int to = 0;
    for (int from = 0; from < vec_t::dim; ++from) {
        if (from == lost_dim) continue;
        projected(to++) = v(from);
    }
    return projected;
}

template <typename vec_t>
mm::Vec<typename vec_t::scalar_t, vec_t::dim + 1> projectVecUp(vec_t v, int gained_dim,
                                                               typename vec_t::scalar_t fixed) {
    mm::Vec<typename vec_t::scalar_t, vec_t::dim + 1> projected;
    int from = 0;
    for (int to = 0; to < vec_t::dim + 1; ++to) {
        if (to == gained_dim)
            projected(gained_dim) = fixed;
        else
            projected(to) = v(from++);
    }
    return projected;
}
template <typename vec_t>
mm::DomainDiscretization<vec_t> variableBox(vec_t beg, vec_t end,
                                            std::function<typename vec_t::scalar_t(vec_t&)> h,
                                            int type_scat = -2,
                                            int type_grid = -3, int seed = -1) {
    return variableBox<vec_t>(
        beg, end, h, [=](vec_t& p) { return false; }, type_scat, type_grid, seed);
}
template <typename vec_t>
mm::DomainDiscretization<vec_t> variableBox(vec_t beg, vec_t end,
                                            std::function<typename vec_t::scalar_t(vec_t&)> h,
                                            std::function<bool(vec_t&)> grid, int type_scat = -2,
                                            int type_grid = -3, int seed = -1) {
    typedef typename vec_t::scalar_t scal_t;
    auto box = mm::BoxShape<vec_t>(beg, end);
    mm::DomainDiscretization<vec_t> domain(box);
    vec_t center = (end - beg) / 2;
    auto corners = cornerPositions(beg, end);
    for (vec_t& c : corners) {
        vec_t normal = ((c.array() > center.array()).template cast<scal_t>().array() * 2 - 1);
        normal.normalize();
        domain.addBoundaryNode(c, type_grid, normal);
    }
    for (int i1 = 0; i1 < corners.size(); ++i1) {
        for (int i2 = 0; i2 < corners.size(); ++i2) {
            if (i1 == i2) continue;
            if ((corners[i1].array() != corners[i2].array()).count() == 1 &&
                (corners[i1].array() < corners[i2].array()).count() == 1) {
                auto pos = discretizeLineWithDensity(corners[i1], corners[i2], h);
                for (int j = 2; j < pos.size(); ++j) {  // first two correspond to the corner nodes
                    vec_t normal = vec_t::Zero();
                    for (int d = 0; d < vec_t::dim; ++d) {
                        if (corners[i1](d) == corners[i2](d)) {
                            normal(d) = mm::signum(pos[j](d) - center(d));
                        }
                    }
                    normal.normalize();
                    domain.addBoundaryNode(pos[j], type_grid, normal);
                }
            }
        }
    }
    // TODO: Consider generalization if/when there is time
    if constexpr (vec_t::dim == 3) {
        for (int d = 0; d < 3; ++d) {
            for (scal_t fixed : {beg(d), end(d)}) {
                mm::DomainDiscretization<mm::Vec2d> face(
                    mm::BoxShape<mm::Vec2d>(projectVecDown(beg, d), projectVecDown(end, d)));
                mm::Vec2d trivial_normal = mm::Vec2d::Constant(1).normalized();
                for (const vec_t& p : domain.positions()) {
                    if (p(d) == fixed)
                        face.addBoundaryNode(projectVecDown(p, d), -1, trivial_normal);
                }
                auto face_h = [&](mm::Vec2d& p) {
                    vec_t p_3d = projectVecUp(p, d, fixed);
                    return h(p_3d);
                };
                auto face_grid = [&](mm::Vec2d& p) {
                    vec_t p_3d = projectVecUp(p, d, fixed);
                    return grid(p_3d);
                };
                mm::HybridFill<mm::Vec2d> hybrid_fill;
                hybrid_fill.seed(seed);
                hybrid_fill(face, face_h, face_grid, -type_scat, -type_grid);
                vec_t normal = vec_t::Zero();
                normal(d) = mm::signum(fixed - center(d));
                for (int i : face.interior()) {
                    domain.addBoundaryNode(projectVecUp(face.pos(i), d, fixed), -face.type(i),
                                           normal);
                }
            }
        }
    }

    return domain;
}
#endif  // ICCS_HYBRID_NODES_DISCRETIZATIONHELPER_HPP
