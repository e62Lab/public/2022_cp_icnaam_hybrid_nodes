#ifndef INC_2023_MIPRO_HYBRID_NODES_CLOVERSHAPE_HPP
#define INC_2023_MIPRO_HYBRID_NODES_CLOVERSHAPE_HPP
#include <medusa/Medusa.hpp>
#include <cmath>
template <typename vector_t>
class CloverShape : public mm::DomainShape<vector_t> {
  public:
    typedef double scalar_t;
    typedef mm::Vec<typename vector_t::scalar_t, vector_t::dim - 1> param_vec_t;
    double _size_scale;
    int _seed;
    // Constructor.
    CloverShape(double size_scale, int seed = 0) : _size_scale{size_scale}, _seed{seed} {
        assert_msg((vector_t::dim == 2) or (vector_t::dim == 3),
                   "Clover shape supported only for 2- or 3-dimensional domains.");
    };
    // Clone.
    CloverShape* clone() const override { return new CloverShape(_size_scale, _seed); }
    // Print.
    std::ostream& print(std::ostream& os) const override { return os << "CloverShape"; }

  public:
    CloverShape& seed(int seed) {
        _seed = seed;
        return *this;
    }
    std::pair<vector_t, vector_t> bbox() const override {
        vector_t lo, hi;
        if constexpr (vector_t::dim == 3) {
            lo = vector_t{clover_param(param_vec_t{3.5084, 1.5708})[0],
                          clover_param(param_vec_t{5.40693, 1.5708})[1],
                          clover_param(param_vec_t{1.57088, 3.14102})[2]};
            hi = vector_t{clover_param(param_vec_t{5.91638, 1.5708})[0],
                          clover_param(param_vec_t{1.5708, 1.5708})[1],
                          clover_param(param_vec_t{3.66516, 0.000254568})[2]};
        } else {
            lo = vector_t{clover_param(param_vec_t{3.52912})[0],
                          clover_param(param_vec_t{3.97146})[1]};
            hi = vector_t{clover_param(param_vec_t{5.89566})[0],
                          clover_param(param_vec_t{mm::PI * 0.5})[1]};
        }
        // Bounds.
        return {lo, hi};
    }
    /**
     * Computes radius.
     * @param angles
     * @return
     */
    double clover_radius(const param_vec_t& angles) const {
        double phi = angles[0];
        double radius;
        if constexpr (vector_t::dim == 3) {
            double theta = angles[1];
            radius = 1.5 -
                     mm::ipow<2>(std::cos(1.5 * (phi - mm::PI / 6))) * theta * (mm::PI - theta) / 3;
        } else {
            radius = 1.5 - mm::ipow<2>(std::cos(1.5 * (phi - mm::PI / 6)));
        }
        radius *= _size_scale / 1.5;  // Scale.
        return radius;
    }
    /**
     * Computes positions as governed by polar/spherical angles.
     * @param angles
     * @return
     */
    vector_t clover_param(const param_vec_t& angles) const {
        double phi = angles[0];
        double r = clover_radius(angles);
        if constexpr (vector_t::dim == 3) {
            double theta = angles[1];
            return vector_t(r * std::cos(phi) * std::sin(theta),
                            r * std::sin(phi) * std::sin(theta), r * std::cos(theta));
        } else {
            return vector_t(r * std::cos(phi), r * std::sin(phi));
        }
    }
    /**
     * Computes Jaccobian matrix.
     * @param angles
     * @return
     */
    Eigen::MatrixXd clover_param_jac(const param_vec_t& angles) const {
        double phi = angles[0];
        Eigen::MatrixXd result(vector_t::dim, param_vec_t ::dim);  // Rows, cols.
        result.setZero();
        int row = 0, col = 0;
        if constexpr (vector_t::dim == 3) {
            double theta = angles[1];
            // row = 0, col = 0
            result(row, col) = -2.0 / 27.0 * _size_scale *
                               ((mm::PI - theta) * theta * std::cos(2.0 * phi) +
                                2.0 * (mm::PI - theta) * theta * std::cos(4.0 * phi) +
                                (9.0 - mm::PI * theta + theta * theta) * std::sin(phi)) *
                               std::sin(theta);
            // row = 1, col = 0
            row = 1;
            col = 0;
            result(row, col) = 2.0 / 27.0 * _size_scale * std::cos(phi) *
                               (9.0 - mm::PI * theta + theta * theta +
                                6.0 * (mm::PI - theta) * theta * std::sin(phi) -
                                4.0 * (mm::PI - theta) * theta * std::sin(3.0 * phi)) *
                               std::sin(theta);
            // row = 2, col = 0
            row = 2;
            col = 0;
            result(row, col) = -2.0 / 9.0 * _size_scale * (mm::PI - theta) * theta *
                               std::cos(3.0 * phi) * std::cos(theta);
            // row = 0, col = 1
            row = 0;
            col = 1;
            result(row, col) =
                -2.0 / 27.0 * _size_scale * std::cos(phi) *
                (std::cos(theta) * (-9.0 + mm::PI * theta - theta * theta +
                                    (mm::PI - theta) * theta * std::sin(3.0 * phi)) +
                 2.0 * (mm::PI - 2.0 * theta) *
                     mm::ipow<2>(std::sin(3.0 * (-2.0 * phi + mm::PI) / 4.0)) * std::sin(theta));
            // row = 1, col = 1
            row = 1;
            col = 1;
            result(row, col) =
                -2.0 / 27.0 * _size_scale * std::sin(phi) *
                (std::cos(theta) * (-9.0 + mm::PI * theta - theta * theta +
                                    (mm::PI - theta) * theta * std::sin(3.0 * phi)) +
                 2.0 * (mm::PI - 2.0 * theta) *
                     mm::ipow<2>(std::sin(3.0 * (-2.0 * phi + mm::PI) / 4)) * std::sin(theta));
            // row = 2, col = 1
            row = 2;
            col = 1;
            result(row, col) = -2.0 / 27.0 * _size_scale *
                               (2.0 * (mm::PI - 2.0 * theta) * std::cos(theta) *
                                    mm::ipow<2>(std::sin((6.0 * phi + mm::PI) / 4.0)) +
                                (9.0 - mm::PI * theta + theta * theta -
                                 (mm::PI - theta) * theta * std::sin(3.0 * phi)) *
                                    std::sin(theta));
        } else {
            row = 0;
            col = 0;
            result(row, col) = -_size_scale * 2.0 / 9.0 *
                               (std::cos(2.0 * phi) + 2.0 * std::cos(4.0 * phi) + 2.0 * std::sin(phi));
            row = 1;
            col = 0;
            result(row, col) = _size_scale * 2.0 / 9.0 *
                               (2.0 * std::cos(phi) + std::sin(2.0 * phi) - 2.0 * std::sin(4.0 * phi));
        }
        return result;
    }
    bool contains(const vector_t& point) const override { return clover_inside(point); }
    /**
     * Inside function.
     * @param p
     * @return
     */
    bool clover_inside(const vector_t& p) const {
        double radius;
        if constexpr (vector_t::dim == 3) {
            double x = p[0];
            double y = p[1];
            double z = p[2];
            double r = p.norm();
            double phi = mm::signum(y) * std::acos(x / std::sqrt(x * x + y * y));
            double theta = std::acos(z / r);
            radius = clover_radius(param_vec_t{phi, theta});
        } else {
            double phi = std::atan2(p[1], p[0]);
            radius = clover_radius(param_vec_t{phi});
        }
        return p.squaredNorm() <= mm::ipow<2>(radius);
    }
    mm::DomainDiscretization<vector_t> discretizeBoundaryWithDensity(
        const std::function<scalar_t(vector_t)>& dr, int type) const override {
        mm::BoxShape<param_vec_t> param_bs(0, 2 * mm::PI);
        if constexpr (vector_t::dim == 3) {
            param_bs = mm::BoxShape<param_vec_t>(0, param_vec_t{2 * mm::PI, mm::PI});
        }
        auto d = mm::DomainDiscretization<vector_t>(*this);
        // Surface fill.
        mm::GeneralSurfaceFill<vector_t, param_vec_t> gsf;
        gsf.seed(_seed);
        // Lambdas.
        auto example_r = [&](param_vec_t angles) { return clover_param(angles); };
        auto example_der = [&](param_vec_t angles) { return clover_param_jac(angles); };
        // Fill.
        d.fill(gsf, param_bs, example_r, example_der, dr, type);
        return d;
    }
};
#endif  // INC_2023_MIPRO_HYBRID_NODES_CLOVERSHAPE_HPP