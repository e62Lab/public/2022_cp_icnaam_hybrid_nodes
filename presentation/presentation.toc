\beamer@sectionintoc {1}{Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Spatially-varying approximation method}{7}{0}{2}
\beamer@sectionintoc {3}{Computational stability}{9}{0}{3}
\beamer@sectionintoc {4}{Governing problem}{11}{0}{4}
\beamer@sectionintoc {5}{Conclusions}{24}{0}{5}
