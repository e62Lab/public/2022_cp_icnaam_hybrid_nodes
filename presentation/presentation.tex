\documentclass[aspectratio=169,10pt]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage[utf8]{inputenc}

\usepackage{booktabs}
\usepackage{physics}
\usepackage[scale=2]{ccicons}
\usepackage{amsmath,amsfonts,amssymb}


\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand{\p}{\emph{p}}
\renewcommand{\r}{\emph{r}}
\newcommand{\h}{\emph{h}}
\newcommand{\hp}{\emph{hp}}
\newcommand{\hr}{\emph{hr}}
\renewcommand{\b}{\boldsymbol}
\newcommand{\lap}{\nabla^2}
\newcommand{\einf}{e_{\infty}}
\newcommand{\n}{\b{n}}
\newcommand{\x}{\b{x}}
\newcommand{\w}{\b{w}}
\newcommand{\T}{\mathsf{T}}
\definecolor{mSybilaRed}{HTML}{990000}

\setbeamercolor{title separator}{
  fg=mSybilaRed
}

\setbeamercolor{background canvas}{bg=white}

\setbeamercolor{progress bar}{%
  fg=mSybilaRed,
  bg=mSybilaRed!90!black!30
}

\setbeamercolor{progress bar in section page}{
  use=progress bar,
  parent=progress bar
}

\setbeamercolor{alerted text}{%
  fg=mSybilaRed
}

\title{Spatially-varying meshless approximation method for enhanced computational efficiency}

\titlegraphic{\hfill\includegraphics[height=1.2cm]{sybila-logo/distComputeLogo.png}}
%\titlegraphic{\hfill\includegraphics[height=0.6cm]{sybila-logo/new.png}}
%\titlegraphic{\hfill\includegraphics[height=0.6cm]{sybila-logo/old.png}}
%\titlegraphic{\hfill\includegraphics[height=0.6cm]{sybila-logo/old-flat.png}}

\date{}
\author{Mitja Jančič, Miha Rot, Gregor Kosec}
\institute{Jožef Stefan Institute, Parallel and Distributed Systems Laboratory  \\ International Postgraduate School Jožef Stefan}

%\title{Metropolis}
% \subtitle{A modern beamer theme for SYBILA}
% \date{\today}
\date{July 4, 2023}
%\author{Matthias Vogelgesang}
% \institute{Center for modern beamer themes}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{sybila-logo/distComputeLogo.png}}

\setbeamertemplate{footline}
{
  \leavevmode
  \hbox{
  \begin{beamercolorbox}[wd=.2\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}
    \usebeamerfont{author in head/foot}\insertshortauthor
  \end{beamercolorbox}

  \begin{beamercolorbox}[wd=.6\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}
    \usebeamerfont{author in head/foot}\insertshorttitle
  \end{beamercolorbox}

  \begin{beamercolorbox}[wd=.2\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}
    \insertframenumber{} / \inserttotalframenumber
  \end{beamercolorbox}
  }
}

\begin{document}

\maketitle

\begin{frame}{Table of contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

%
%
\section{Motivation}

\begin{frame}[fragile]{Why numerical solution?}
  \begin{columns}[T,onlytextwidth]
    \column{0.5\textwidth}
    Realistic problems do not have closed form solutions.
    \begin{align*}
      \div \vec{v}                                    & = 0,                                                                                  \\
      \pdv{\vec{v}}{t} + \vec{v} \cdot \grad{\vec{v}} & = -\grad p +\div(\mathrm{Ra} \grad \vec{v}) - \vec{g}\mathrm{Ra}\mathrm{Pr} T_\Delta, \\
      \pdv{T}{t} + \vec{v} \cdot \grad{T}             & = \div( \grad
      T)
    \end{align*}

    \column{0.4\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth]{figures/sketches.png}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Numerical treatment}
  \begin{columns}[T,onlytextwidth]
    \column{0.5\textwidth}
    Numerical treatment is required:
    \begin{enumerate}
      \item Domain discretization
      \item Differential operator approximation
      \item PDE discretization
      \item Solve sparse linear system
    \end{enumerate}
    \metroset{block=fill}
    \begin{block}{Differential operator approximation}
      \begin{align*}
        (\mathcal L u)(\boldsymbol x_c)     & \approx \sum_{i=1}^n w_i u(\boldsymbol x_i)       \\
        \mathcal L \Big |_{\boldsymbol x_c} & = \boldsymbol w_{\mathcal L} (\boldsymbol x_c)^ T
      \end{align*}
    \end{block}
    \column{0.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=0.6\textwidth]{figures/mesh.png}
      \includegraphics[width=0.6\textwidth]{figures/grid.png}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{Approximation basis}
  \begin{columns}[T,onlytextwidth]
    \column{0.48\textwidth}
    \metroset{block=fill}
    \begin{block}{RBF-FD}
      \begin{itemize}
        \item Polyharmonic splines augmented with monomials
        \item[+] Higher stability
        \item[+] Stable on scattered nodes
        \item[-] Computationally demanding
      \end{itemize}
      Stencil size: 12 in 2D, 20 in 3D
    \end{block}
    \column{0.48\textwidth}
    \metroset{block=fill}
    \begin{block}{MON}
      \begin{itemize}
        \item A set of monomials centred at stencil nodes.
        \item[+] Computationally cheap
        \item[-] Unstable on irregular nodes
      \end{itemize}
      Stencil size: 5 in 2D and 7 in 3D
    \end{block}
  \end{columns}
  \vspace{1cm}
  \metroset{block=fill}
  \setbeamercolor{block body}{bg=green!10}
  \begin{block}{\vspace*{-3.3ex}}
    \textbf{Both} allow control over the order of the approximation.
  \end{block}
\end{frame}

\section{Spatially-varying approximation method}

\begin{frame}[fragile]{Spatially-varying approximation method}
  \begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{../manuscript/images/irregular_domain.eps}
  \end{figure}
\end{frame}


%
%
\section{Computational stability}
\begin{frame}[fragile]{Computational stability}
  The stability is estimated via the condition number $\kappa(\mathbf{M}) = \left \| \mathbf{M} \right \| \left \| \mathbf{M}^{-1} \right \|$, where $\left \| \cdot \right \|$ denotes the $L^2$ norm.

  \begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{../manuscript/images/irregular_domain_condition_number.eps}
  \end{figure}
\end{frame}

%
%
\section{Governing problem}
\begin{frame}[fragile]{Governing problem}
  To assess the advantages of the hybrid method, we focus on the \underline{natural convection problem} that is governed by a system of three PDEs that describe the continuity of mass, the conservation of momentum and the transfer of heat
  \begin{align}
    \div \vec{v}                                    & = 0, \label{eq:physics1}                                                                                  \\
    \pdv{\vec{v}}{t} + \vec{v} \cdot \grad{\vec{v}} & = -\grad p +\div(\mathrm{Pr} \grad \vec{v}) -\mathrm{Ra}\mathrm{Pr} \vec{g} T_\Delta, \label{eq:physics2} \\
    \pdv{T}{t} + \vec{v} \cdot \grad{T}             & = \div( \grad T), \label{eq:physics3}
  \end{align}
  where a dimensionless nomenclature using Rayleigh (Ra) and Prandtl (Pr) numbers is used.
\end{frame}

\begin{frame}{The de Vahl Davis problem}
  \begin{itemize}
    \item To establish confidence in the presented solution procedure.
    \item Because the regularity of the domain shape allows us to efficiently discretize it using exclusively scattered or regular nodes.
  \end{itemize}

  \begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{../manuscript/images/dvd_sketch.pdf}
    \includegraphics[width=0.38\textwidth]{../manuscript/images/dvd_domain.eps}
  \end{figure}
\end{frame}

\begin{frame}{The de Vahl Davis problem: example solution}
  \begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{../manuscript/images/dvd_solution.eps}
  \end{figure}
\end{frame}

\begin{frame}{The de Vahl Davis problem: Nusselt number}
  Nusselt number: a convenient scalar value for comparison with reference solutions. The average Nusselt number ($\overline{\mathrm{Nu}}$) is calculated as the average of the Nusselt values at the cold wall nodes
  \begin{equation}
    \mathrm{Nu} = \frac{L}{T_H-T_C}\abs{\pdv{T}{\boldsymbol n}}_{x=0}.
  \end{equation}
  \begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{../manuscript/images/dvd_nusselt_time.eps}
  \end{figure}
\end{frame}

\begin{frame}{The de Vahl Davis problem: Comparing with references}
  \begin{table}
    \centering
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{cccc}
      Approximation              & $\overline{\text{Nu}}$ & execution time [h] & $N$     \\ \hline \hline
      scattered                  & 8.867                  & 6.23               & 55\,477 \\
      regular                    & 8.852                  & 2.42               & 64\,005 \\
      hybrid                     & 8.870                  & 3.11               & 59\,694 \\ \hline
      Kosec and Šarler (2007)    & 8.97                   & /                  & 10201   \\
      Sadat and Couturier (2000) & 8.828                  & /                  & 22801   \\
      Wan et.~al.~(2001)         & 8.8                    & /                  & 10201   \\
    \end{tabular}
    \caption{Average Nusselt along the cold edge along with execution times and number of discretization nodes.}
    \label{tab:nusselt}
  \end{table}
  \begin{itemize}
    \item The hybrid method shows significantly shorter computational time (about 50 \%), than that required by the scattered discretization employing RBF-FD.
  \end{itemize}
\end{frame}

\begin{frame}{The de Vahl Davis problem: Convergence analysis}
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../manuscript/images/dvd_conv.eps}
  \end{figure}
\end{frame}

\begin{frame}{The de Vahl Davis problem: Width of scattered layer $\delta_h$}
  The domain is split into two parts at a distance $h\delta_h$ from the origin in the lower left corner.
  \begin{itemize}
    \item \textbf{Horizontal split}, resulting in scattered nodes below the imaginary split and regular nodes above it.
    \item \textbf{Vertical split}, resulting in scattered nodes to the left of it and regular nodes to the right of it.
  \end{itemize}

  \begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{../manuscript/images/dvd_nusselt_delta_h.eps}
    \caption{Demonstration of the scattered node layer width ($\delta_h$) effect on the accuracy of the numerical solution.}
    \label{fig:dvd_delta}
  \end{figure}
\end{frame}


\begin{frame}{Natural convection on irregularly shaped domains}
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../manuscript/images/irregular_solution.eps}
  \end{figure}
\end{frame}

\begin{frame}{Natural convection on irregularly shaped domains: Nusselt number}

  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../manuscript/images/duck_nusselt_and_delta_h.eps}
  \end{figure}
\end{frame}

\begin{frame}{Natural convection on irregularly shaped domains: Computational times}
  The hybrid method effectively reduces the execution time for approximately 35~\%. The pure regular discretization with MON approximation is omitted from the table because a stable numerical solution could not be obtained.

  \begin{table}[h]
    \centering
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{cccc}
      Approximation & $\overline{\text{Nu}}$ & execution time [min] & $N$     \\ \hline \hline
      scattered     & 12.32                  & 46.31                & 10\,534 \\
      hybrid        & 12.36                  & 29.11                & 11\,535 \\
    \end{tabular}
    \caption{Average Nusselt along the cold duck edges along with execution times. Note that all values in the table were obtained for $\delta_h = 4$.}
    \label{tab:nusselt_ireg}
  \end{table}
\end{frame}

\begin{frame}{Application to three-dimensional irregular domains}
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{../manuscript/images/irregullar_3d_solution_mlab_third.png}
  \end{figure}
\end{frame}

\begin{frame}{Application to three-dimensional irregular domains: Execution times}
  The scattered method took about 48 hours and the hybrid scattered-regular approximation method took 20 hours to simulate 1 dimensionless time unit with the dimensionless time step $\mathrm{d}t = 7.8125\cdot10^{-6}$ and about 75\,000 computational nodes with $\delta_h = 4$.

  \begin{table}[h]
    \centering
    \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{cccc}
      Approximation & $\overline{\text{Nu}}$ & execution time [h] & $N$     \\ \hline \hline
      scattered     & 7.36                   & 48.12              & 65\,526 \\
      hybrid        & 6.91                   & 20.54              & 74\,137 \\
    \end{tabular}
    \caption{Average Nusselt along the cold spheres, execution time, and number of computational nodes.}
    \label{tab:nusselt_ireg_3d}
  \end{table}
  \emph{Note:} The pure regular discretization with MON approximation is again omitted from the table because a stable numerical solution could not be obtained.
\end{frame}


\section{Conclusions}

\begin{frame}{Summary}
  \begin{columns}[T,onlytextwidth]
    \column{0.48\textwidth}
    \begin{itemize}
      \item Proposed a computationally efficient approach to the numerical treatment of PDEs.
      \item Verified on a solution to a two-dimensional de Vahl Davis natural convection problem.
      \item Demonstrated on a solution within irregular two- and three-dimensional domains.
    \end{itemize}
    \column{0.48\textwidth}
    \underline{Future work:}
    \begin{itemize}
      \item[$\star$] A detailed study of the width of the scattered layer $\delta_h$.
      \item[$\star$] Further study of aggressiveness of $h$-refinement.
      \item[$\star$] Demonstration on more difficult problems: Mixed convection.
    \end{itemize}
  \end{columns}
\end{frame}


\begin{frame}[standout]
  Thank you for your attention. 
  \newline
  Questions?
\end{frame}

\end{document}
