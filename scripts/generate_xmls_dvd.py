import xml.etree.ElementTree as ET
import numpy as np

file_path = "../input/"
master_file = "devahldavis_quads.xml"

# Read master file
tree = ET.parse(file_path + master_file)
root = tree.getroot()

# Manipulate XML and save
types = ['hybrid', 'scattered', 'uniform']
# hs = np.logspace(-1.92, -2.4, 6)
hs = [1 / n for n in np.geomspace(75, 275, 7, dtype='int')]
i = 0
for type in types:
    for j, h in enumerate(hs):
        root.find("domain").set("type", "{}".format(type))
        root.find("num").set("h", "{}".format(h))

        # General.
        root.find("output").set('hdf5_name', f'results_{type}_h{j}.h5')
        tree.write('../input/dvd_timings/results_{}_h{}.xml'.format(type, j))
        i += 1

print('Created ' + str(i) + ' files.')
