import xml.etree.ElementTree as ET
import numpy as np

file_path = "../input/"
master_file = "bou.xml"

# **********
# UNREFINED
# **********
eps = 0.1
hs = [(1 - eps) / n for n in np.linspace(30, 60, 10, dtype='int')]
num_threads = 3
verbosity = 0
discretization_types = ['fdm', 'scattered', 'hybrid']

i = 0
for h in hs:
    for d_type in discretization_types:
        # Read master file
        tree = ET.parse(file_path + master_file)
        root = tree.getroot()

        # General.
        root.find("sys").set("num_threads", "{}".format(num_threads))
        root.find("output").set("verbosity", "{}".format(verbosity))
        root.find("domain").set("eps", "{}".format(eps))

        # Specific.
        root.find("domain").set("type", "{}".format(d_type))
        root.find("num").set("h_large", "{}".format(h))
        root.find("num").set("h_min", "{}".format(h))

        root.find("output").set('hdf5_name', '../data/bou_unrefined_{:02d}.h5'.format(i))
        tree.write('../input/bou_timings/bou_settings_{:02d}.xml'.format(i))
        i += 1

print('Created ' + str(i) + ' files for unrefined scan.')

# **********
# EPS scan
# **********
epsilons = np.linspace(0.01, 0.1, 10)
num_threads = 1
verbosity = 0
discretization_types = ['fdm', 'scattered', 'hybrid']

i = 0
for eps in epsilons:
    for d_type in discretization_types:
        # Read master file
        tree = ET.parse(file_path + master_file)
        root = tree.getroot()

        # General.
        root.find("sys").set("num_threads", "{}".format(num_threads))
        root.find("output").set("verbosity", "{}".format(verbosity))

        # Specific.
        root.find("domain").set("eps", "{}".format(eps))
        root.find("domain").set("type", "{}".format(d_type))
        root.find("num").set("h_large", "{}".format((1 - eps) / 45))
        root.find("num").set("h_min", "{}".format((1 - eps) / 45 / 5))

        root.find("output").set('hdf5_name', '../data/bou_eps_{:02d}.h5'.format(i))
        tree.write('../input/bou/settings_eps_{:02d}.xml'.format(i))
        i += 1

print('Created ' + str(i) + ' files for eps scan.')

# **********
# REFINE scan
# **********
eps = 0.1
num_threads = 1
h_large = (1 - eps) / 40
h_ratios = np.linspace(1, 15, 10)
verbosity = 0
discretization_types = ['scattered', 'hybrid']

i = 0
for h_ratio in h_ratios:
    for d_type in discretization_types:
        # Read master file
        tree = ET.parse(file_path + master_file)
        root = tree.getroot()

        # General.
        root.find("sys").set("num_threads", "{}".format(num_threads))
        root.find("output").set("verbosity", "{}".format(verbosity))
        root.find("domain").set("eps", "{}".format(eps))

        # Specific.
        root.find("domain").set("type", "{}".format(d_type))
        root.find("num").set("h_large", "{}".format(h_large))
        root.find("num").set("h_min", "{}".format(h_large / h_ratio))

        root.find("output").set('hdf5_name', '../data/bou_refine_{:02d}.h5'.format(i))
        tree.write('../input/bou/settings_refine_{:02d}.xml'.format(i))
        i += 1

print('Created ' + str(i) + ' files for refined scan.')

#####
# REFINED CONVERGENCE
#####

# **********
# TEST scan
# **********
eps = 0.1
num_threads = 2
hs_large = [(1 - eps) / n for n in np.linspace(30, 50, 5, dtype='int')]
verbosity = 0

configs = [['scattered', [0.4], [8]],
           ['hybrid', [0.4, 0.3, 0.2], [8, 5, 3]]]

i = 0
for conf in configs:
    d_type = conf[0]
    radii = conf[1]
    h_ratios = conf[2]


    if (len(radii) != len(h_ratios)):
        print("Something went wrong!")
        break
    for radius, h_ratio in zip(radii, h_ratios):
        for h_large in hs_large:
            # Read master file
            tree = ET.parse(file_path + master_file)
            root = tree.getroot()

            # General.
            root.find("sys").set("num_threads", "{}".format(num_threads))
            root.find("output").set("verbosity", "{}".format(verbosity))
            root.find("domain").set("eps", "{}".format(eps))

            # Specific.
            root.find("domain").set("type", "{}".format(d_type))
            root.find("domain").set("corner_radius", "{}".format(radius))
            root.find("num").set("h_large", "{}".format(h_large))
            root.find("num").set("h_min", "{}".format(h_large / h_ratio))

            root.find("output").set('hdf5_name', '../data/bou_refined_convergence_{:02d}.h5'.format(i))
            tree.write('../input/bou_timings/settings_refined_conv_{:02d}.xml'.format(i))
            i += 1

print('Created ' + str(i) + ' files for refined conv scan.')
