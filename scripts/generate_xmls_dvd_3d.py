import xml.etree.ElementTree as ET
import numpy as np

file_path = "../input/"
master_file = "devahldavis_quads_dim3.xml"

# Read master file
tree = ET.parse(file_path + master_file)
root = tree.getroot()

# Manipulate XML and save
types = ['hybrid', 'scattered', 'uniform']
# hs=np.logspace(-1.3, -1.57, 6)
hs = 1 / np.array([35, 40, 45, 50, 55, 60])
i = 0
for type in types:
    for j, h in enumerate(hs):
        root.find("domain").set("type", "{}".format(type))
        root.find("num").set("h", "{}".format(h))

        # General.
        root.find("output").set('hdf5_name', f"results_{type}_h{j}.h5")
        tree.write('../input/dvd_3d_timings/results_{}_h{}.xml'.format(type, j))
        i += 1

print('Created ' + str(i) + ' files.')
