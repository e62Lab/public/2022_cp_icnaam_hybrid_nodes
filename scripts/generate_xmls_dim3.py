import xml.etree.ElementTree as ET
import numpy as np

file_path = "../input/"
master_file = "irregular_dim3.xml"

# Manipulate XML and save
hs = [1 / n for n in np.linspace(20, 45, 10, dtype='int')]
delta_h = 4
num_threads = 3
verbosity = 0
discretization_types = ['fdm', 'scattered', 'hybrid']
i = 0
for h in hs:
    for d_type in discretization_types:
        # Read master file
        tree = ET.parse(file_path + master_file)
        root = tree.getroot()

        # General.
        root.find("domain").set("scattered_h", "{}".format(delta_h))
        root.find("sys").set("num_threads", "{}".format(num_threads))
        root.find("output").set("verbosity", "{}".format(verbosity))

        # Specific.
        root.find("domain").set("type", "{}".format(d_type))
        root.find("num").set("h", "{}".format(h))
        root.find("num").set("sphere_h", "{}".format(h))

        root.find("output").set('hdf5_name', '../data/irregular_unrefined_dim3_{:02d}.h5'.format(i))
        tree.write('../input/irregular_dim3_timings/settings_{:02d}.xml'.format(i))
        i += 1

print('Created ' + str(i) + ' files for unrefined scan.')

# GENERATE FOR REFINE ANALYSIS
# Manipulate XML and save
h_large = 1 / 35
h_ratios = np.arange(2, 7)
deltas_h = [2, 5, 10]
num_threads = 2
verbosity = 0
output_count = 10
discretization_types = ['hybrid']
i = 0
for delta_h in deltas_h:
    for d_type in discretization_types:
        for h_ratio in (h_ratios if d_type != "fdm" else [1]):
            # Read master file
            tree = ET.parse(file_path + master_file)
            root = tree.getroot()
            # General.
            root.find("sys").set("num_threads", "{}".format(num_threads))
            root.find("output").set("verbosity", "{}".format(verbosity))
            root.find("output").set("count", "{}".format(output_count))
            root.find("num").set("h", "{}".format(h_large))

            # Specific.
            root.find("domain").set("scattered_h", "{}".format(delta_h))
            root.find("num").set("sphere_h", "{}".format(h_large / h_ratio))
            root.find("domain").set("type", "{}".format(d_type))

            root.find("output").set('hdf5_name', '../data/irregular_refine_dim3_{:03d}.h5'.format(i))
            tree.write('../input/irregular_dim3/settings_refine_{:03d}.xml'.format(i))
            i += 1

print('Created ' + str(i) + ' files for refine scan.')

# GENERATE FOR delta_h analysis
h_large = 1 / 35
h_ratios = [2, 4, 6]
deltas_h = np.arange(2, 20, 2)
num_threads = 2
verbosity = 0
discretization_types = ['hybrid']
i = 0
output_count = 10
for h_ratio in h_ratios:
    for delta_h in deltas_h:
        for d_type in discretization_types:
            # Read master file
            tree = ET.parse(file_path + master_file)
            root = tree.getroot()
            # General.
            root.find("sys").set("num_threads", "{}".format(num_threads))
            root.find("output").set("verbosity", "{}".format(verbosity))
            root.find("output").set("count", "{}".format(output_count))
            root.find("num").set("h", "{}".format(h_large))

            # Specific.
            root.find("domain").set("type", "{}".format(d_type))
            root.find("num").set("sphere_h", "{}".format(h_large / h_ratio))
            root.find("domain").set("scattered_h", "{}".format(delta_h))

            root.find("output").set('hdf5_name', '../data/irregular_refine_deltah_dim3_{:03d}.h5'.format(i))
            tree.write('../input/irregular_dim3/settings_{:03d}.xml'.format(i))
            i += 1

print('Created ' + str(i) + ' files for deltah scan.')
