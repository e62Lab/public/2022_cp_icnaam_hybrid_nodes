import xml.etree.ElementTree as ET
import numpy as np

file_path = "../input/"
master_file = "clover.xml"

# GENERATE FOR UNREFINED ANALYSIS
# Manipulate XML and save
hs = [1 / n for n in np.linspace(100, 325, 8, dtype='int')]
delta_h = 4
num_threads = 1
verbosity = 2
discretization_types = ['scattered', 'hybrid']

i = 0
n = 0
for h in hs:
    for d_type in discretization_types:
        # Read master file
        tree = ET.parse(file_path + master_file)
        root = tree.getroot()

        # General.
        root.find("domain").set("scattered_h", "{}".format(delta_h))
        root.find("sys").set("num_threads", "{}".format(num_threads))
        root.find("output").set("verbosity", "{}".format(verbosity))

        # Specific.
        root.find("domain").set("type", "{}".format(d_type))
        root.find("num").set("h", "{}".format(h))
        root.find("num").set("sphere_h", "{}".format(h))

        root.find("output").set('hdf5_name', f'dim2_unrefined_h{i}_{d_type}.h5')
        tree.write(f'../input/irregular_unrefined_timings/dim2_unrefined_h{i}_{d_type}.xml')
        n += 1
    i += 1

print('Created ' + str(n) + ' files for unrefined.')

# GENERATE FOR REFINE ANALYSIS
# Manipulate XML and save
h_small = 1 / 200
h_ratios = np.linspace(1, 8, 15)
deltas_h = [2, 4, 10]
num_threads = 1
verbosity = 0
discretization_types = ['hybrid']
output_count = 10
i = 0
for delta_h in deltas_h:
    for d_type in discretization_types:
        for h_ratio in h_ratios:
            # Read master file
            tree = ET.parse(file_path + master_file)
            root = tree.getroot()
            # General.
            root.find("sys").set("num_threads", "{}".format(num_threads))
            root.find("output").set("verbosity", "{}".format(verbosity))
            root.find("output").set("count", "{}".format(output_count))

            # Specific.
            root.find("num").set("h", "{}".format(h_small * h_ratio))
            root.find("domain").set("scattered_h", "{}".format(delta_h))
            root.find("num").set("sphere_h", "{}".format(h_small))
            root.find("domain").set("type", "{}".format(d_type))

            root.find("output").set('hdf5_name', f'dim2_delta{delta_h}ratio{str(h_ratio).replace(".", "-")}.h5')
            tree.write(f'../input/delta_analysis/dim2_delta{delta_h}ratio{str(h_ratio).replace(".", "-")}.xml')
            i += 1

print('Created ' + str(i) + ' files for refine scan.')

# # GENERATE FOR delta_h analysis
# h_large = 1 / 140
# h_ratios = [2, 4, 6]
# deltas_h = np.arange(2, 30, 2)
# num_threads = 4
# verbosity = 0
# discretization_types = ['hybrid']
# output_count = 10
# i = 0
# for h_ratio in h_ratios:
#     for delta_h in deltas_h:
#         for d_type in discretization_types:
#             # Read master file
#             tree = ET.parse(file_path + master_file)
#             root = tree.getroot()
#             # General.
#             root.find("sys").set("num_threads", "{}".format(num_threads))
#             root.find("output").set("verbosity", "{}".format(verbosity))
#             root.find("output").set("count", "{}".format(output_count))
#             root.find("num").set("h", "{}".format(h_large))
#
#             # Specific.
#             root.find("domain").set("type", "{}".format(d_type))
#             root.find("num").set("sphere_h", "{}".format(h_large / h_ratio))
#             root.find("domain").set("scattered_h", "{}".format(delta_h))
#
#             root.find("output").set('hdf5_name', '../data/irregular_dim2_deltah_{:03d}.h5'.format(i))
#             tree.write('../input/irregular_dim2/settings_deltah_{:03d}.xml'.format(i))
#             i += 1
#
# print('Created ' + str(i) + ' for deltah scan.')

# **********
# REFINED CONVERGENCE
# **********
eps = 0.1
num_threads = 1
hs_small = [1 / n for n in np.linspace(100, 350, 7, dtype='int')]
verbosity = 0
output_count = 100
support = -1
configs = [['scattered', [3], [2]],
           ['hybrid', [3, 5], [2, 5]]]
n = 0
for conf in configs:
    d_type = conf[0]
    deltas_h = conf[1]
    h_ratios = conf[2]


    if (len(deltas_h) != len(h_ratios)):
        print("Something went wrong!")
        break
    for delta_h, h_ratio in zip(deltas_h, h_ratios):
        i=0
        for h_small in hs_small:
            # Read master file
            tree = ET.parse(file_path + master_file)
            root = tree.getroot()
            # General.
            root.find("sys").set("num_threads", "{}".format(num_threads))
            root.find("output").set("verbosity", "{}".format(verbosity))
            root.find("output").set("count", "{}".format(output_count))
            root.find("approx_collocation").set("support_size", "{}".format(support))

            # Specific.
            root.find("domain").set("type", "{}".format(d_type))
            root.find("domain").set("scattered_h", "{}".format(delta_h))
            root.find("num").set("h", "{}".format(h_small * h_ratio))
            root.find("num").set("sphere_h", "{}".format(h_small))

            root.find("output").set('hdf5_name', f'dim2_refined_h{i}_delta{delta_h}_ratio{h_ratio}_{d_type}.h5')
            tree.write(f'../input/irregular_refined_timings/dim2_refined_h{i}_delta{delta_h}_ratio{h_ratio}_{d_type}.xml')
            n += 1
            i += 1

print('Created ' + str(n) + ' files for refined conv scan.')
